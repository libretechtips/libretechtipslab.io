---
title: "About me"
subtitle: "About me"
comments: false
date: 2019-07-01
showthedate: false
---

I've been using Linux on the desktop since about 2001, and Linux on a server since 2010. I am the creator of a free Linux spin based on openSUSE called [GeckoLinux](http://geckolinux.github.io/), which is what I currently use on a daily basis. I also have experience with the Linux / Nginx / MariaDB / PHP-FPM stack ("LEMP") for high performance websites. I am the owner and administrator of one of the largest internet forums running on the Drupal CMS. Additionally, I have experience with a wide range of Linux desktop and web based productivity tools and search engines.

I always prefer open source solutions whenever available and feasible, and I value my personal and online privacy and security. At the same time I am a pragmatist, and I use proprietary solutions where necessary to get work done.

### Questions or Comments?
https://gitlab.com/libretechtips/libretechtips.gitlab.io/issues
