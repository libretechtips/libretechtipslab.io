---
title: "MSI Modern 14 (B10MW-486 / Modern14486) review: A decent and affordable Linux compatible laptop"
slug: msi-modern-14_b10MW-486_modern14486_review
date: 2021-09-17
tags: ["hardware", "Linux", "reviews"]
---

![MSI Modern 14 front view](https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6452/6452969_sd.jpg)

It's been over a year since the last [article](https://libretechtips.gitlab.io/a-disenchanted-thinkpad-user-looks-for-a-quality-linux-laptop/) about my search for a decent laptop that is highly Linux compatible. During that time, I purchased and returned not one but _two_ defective laptops from a Linux laptop manufacturer, one of which had to be returned _two times_. The company is a perfectly legitimate enterprise and was very decent in its handling of all the issues that came up, so it will remain unnamed. But let's just say that this frustrating experience only reinforced my opinion that all modern laptop hardware (and indeed most hardware in general) these days is total garbage. Enter the proponent of this article: for the next attempt at meeting my need for a secondary portable laptop I finally settled on the MSI Modern 14 ([B10MW-486](https://storage-asset.msi.com/specSheet/us/bnp/Modern%2014%20B10MW-486.pdf)) unit. It is by no means a perfect laptop, but it does offer some unique benefits that make it worthy of consideration by Linux users.

----------

### Pros:
- Perfect Linux compatibility
- Very competitively priced
- Good FHD screen
- Aluminum construction with good build quality
- Accessible and upgradable components
- Acceptable aesthetics and good portability
- Acceptable performance and battery life
- Quiet operation

### Cons:
- Bewildering array of similar models with differing specs
- Senseless cost-cutting measures
- Idiotic keyboard layout
- Poor speaker(s)
- Few and slow USB 2.0 ports
- No ethernet port

-----------

#### Possible Scoring Table:

- ⭐⭐⭐⭐ **= Excellent**
- ⭐⭐⭐☆ **= Good**
- ⭐⭐☆☆ **= Acceptable**
- ⭐☆☆☆ **= Poor**
- ☆☆☆☆ **= Unacceptable** 

-----------

## Value and Availability: ⭐⭐⭐⭐

There really isn't any competition for the MSI Modern 14. Since I will only be using it as a secondary highly portable laptop I didn't need large amounts of memory or processing power. So I simply added a few basic filters to the laptop categories of multiple online retailers: 8GB of RAM and a 14" FHD screen with solid state storage for less than US$500. Now, there are of course countless laptops that fit those basic criteria, but all of them had serious issues. HP seems to have the largest number of budget laptops in this category, but I ignored them all because every HP laptop that I've ever seen or used has been junk. Dell tends to have clunky, stodgy designs and aggressive cost-cutting in the screen quality, with quite a few models still offering a wretched 1366x768 "HD" display. I actually ordered and then cancelled the order for a Lenovo IdeaPad 14" with a Ryzen CPU. It appears to have a fairly attractive design and incredible processing power for the price, but I later discovered that Lenovo is increasingly using extremely cheap Realtek WiFI chipsets with poor Linux support and glitchy performance even on Windows. The same goes for the Asus VivoBook lineup. Another major downside of the Lenovo IdeaPad that I cancelled is the fact that it has one soldered 4GB RAM stick, and another slot that can only accept up to 8GB, making the total RAM capacity a puzzling maximum of only 12GB, which relegates its fantastic processing power to little more than bragging rights. Plus, the Vega graphics has no dedicated memory and uses a considerable chunk of the 8GB of RAM that the system comes with. Acer didn't have any attractive models in this price range, and their compatibility with Linux also tends to be marginal. 

I had never considered MSI, since in my mind it was an ancient producer of some of the first laptops ever made many decades ago, and more recently it has come to be associated more with hardcore gaming. But it turns out that they also have several product lines oriented toward business and general users. The specs of the MSI Modern 14 (B10MW-486) are nothing to write home about, although MSI does offer many other variations in the Modern 14 lineup with much higher specs, including models with powerful Ryzen mobile chipsets or Intel 11th gen hardware with Iris graphics. In my price range, however, I had to settle for a humble Intel Core i3-10110U processor and 8GB of RAM, which I consider to be the bare minimum for normal workloads these days. But for a secondary laptop that still suits my needs, and given the small battery capacity of this particular model it's just as well that it doesn't have a more power-hungry processor. Fortunately in the other really important aspects of the laptop MSI didn't skimp excessively, with a good FHD 1920x1080 display and solid aluminum construction, which is almost impossible to find in this price bracket. And the deciding factor for me after discovering about Lenovo's Realtek WiFI woes is the fact that all the MSI Modern 14 submodel permutations ship with Intel 9560 Jefferson Peak (2x2 802.11 ac) WIFI, which is both performant and reliable _and_ perfectly compatible with Linux. 

But going back to the Modern 14 model lineup, the WiFI seems to be the only consistent spec. A considerable downside for potential online buyers is the plethora of submodels with highly variable specifications. This [beauty of a webpage](https://us.msi.com/Business-Productivity/Modern-14-B10X-r/Specification) doesn't help things either, and then that can easily be confused with the [latest generation model spread](https://www.msi.com/Business-Productivity/Modern-14-B11X/Specification), which is also still called "Modern 14". Notice how they have a specific model number (such as "B10MW-486") and then also a "part number" (like "Modern14486"). So this obviously creates a nightmare when creating a product listing even for honest sellers and retailers, and in my case it got me a laptop with significantly less battery capacity than I expected. The item I purchased was listed as a "Modern14B486" (notice the 'B'), which doesn't even exist on the aforementioned MSI webpages, but based on [this](https://www.bestbuy.com/site/msi-modern-14-laptop-intel-i3-8gb-memory-128gb-ssd-black/6452969.p?skuId=6452969&intl=nosplash) I assumed it was a special model exclusive to BestBuy, claiming to have a 52Whr battery. But when I opened the box I found that it was indeed the "B10MW-486" model listed on MSI's website with a mere 39Whr battery. So definitely check and double check what you're buying, and don't assume anything based on reviews that you read unless they show the specific model and also corroborate it with the part number. With that being said, some form of the Modern 14 is readily available at brick'n'mortar stores and all over online. Prices are all over the place, but if you search a bit you can easily find the cheaper spec'd models at around $400 or even less.


## Linux Compatibility: ⭐⭐⭐⭐

This is a huge advantage to the Modern 14, or at least with the specific model I have and probably in all the models in this generation with all-Intel components. Although the specs aren't too exciting, the advantage of Intel is its generally flawless Linux compatibility. I was pleasantly surprised after inserting a live USB that it booted Linux immediately without having to mash the `Fn` keys or defeat the stupid "Secure Boot" facade as almost always happens these days with modern Windows laptops. After that good start, I was even happier to discover that Linux support is essentially flawless with this laptop, without any nonsense. Everything works as expected out of the box. There was a small issue with not being able to disable Bluetooth, but that was specific to the very old Linux 5.3 kernel used in the openSUSE Leap 15.3 based system that I installed, and in the newer kernels found in most distros the issue does not exist. So out-of-the-box Linux compatibility with the MSI Modern 14 is easily as good as or even better than most hardware offered by the dedicated Linux laptop vendors, and at a considerably more attractive price.


## Design and Hardware Quality: ⭐⭐⭐☆

Initial quality impressions are good. As mentioned, the all-aluminum construction is very unique at this price point, although the decision to paint a genuine aluminum laptop the color of cheap black plastic was a strange decision. This machine would be much more attractive with a natural aluminum anodized finish or a light gunmetal color instead of dark charcoal. It does feel reasonably solid and well built, although long-term durability is anybody's best guess given the abysmal quality of electronics these days.

The MSI Modern 14 is highly portable, basically a perfect design for my intended use of it as a secondary mobile laptop. It is lightweight at 1282g or 2.8lbs. It is only a few centimeters thick when closed, without much excess girth on the case. And another positive quality is the compact charger to complement the portable nature of this machine.

Speaking of the charger, it is worth mentioning that it has a proper 3-pronged grounded plug. This small detail is important for use in buildings with electrical interference from a ground loop, which even causes problems with USB recording devices. This is a major issue with the entire Lenovo Thinkpad lineup for example, which all have ungrounded AC plugs. A proper 3-pronged plug like the Modern 14 charger uses eliminates the ground loop problem. A less positive aspect of the charger is the ridiculously short AC cord, and the also rather short DC cord on the other side. This is an example of senseless and aggressive cost-cutting that only causes annoyances for the user and can't possibly save more than 50 cents on each unit.

The screen is another important pro of this laptop. It is "Full HD (FHD)" with the standard 1920x1080 resolution for that designation, which is a significant step up from the garbage "HD" 1366x768 resolution that some machines in this category still have. Screen brightness is acceptable, not exceptional, but more than enough for use even in an office illuminated by bright sun shining through large windows. Viewing angles are quite good, with minimal color distortion around the axis of the screen tilt and toward both sides of the screen. The screen illumination does not appear to use PWM at lower brightness levels, which is commendable. From an aesthetics point of view, there's something about the back of the open screen acting as a prop hanging below the keyboard deck level that doesn't look good to my eyes when viewed from the side profile. But it is functional and stable, and the screen does not jiggle while typing. The screen bezels are rather narrow at this price point. It would look even more premium if the screen had a flush cover, but that would probably cause glare. As it is, the screen has a matte finish and is reasonably free of glare.

The touchpad is appropriately sized and doesn't get in the way while typing; fortunately they didn't fall for the trap of imitating Apple with an oversized trackpad. It has no physical buttons, but the amount of force required to depress it for a physical click is just about right, and it feels sturdy and stable. I can't tell if it's glass or plastic, but it feels good to the touch and tracks smoothly.

The webcam is the standard fare offering found in most laptops, neither good nor especially poor image quality. It has a tiny slow-flashing white LED indicator when active. Fortunately it is located where it belongs at the top of the screen.

The integrated microphone is good. It appears to have two of them located toward the top of the screen for noise reduction. The combined headphone/microphone 3.5mm jack works correctly with Linux and appears to have minimal electrical noise when using cellphone earbuds with an inline microphone.

Speaker quality is quite poor. Don't fall for the "Nahimic 3 Hi-Res Audio" branding hype; as a matter of fact "Anemic" would be a more appropriate moniker. The speakers are down-firing, sounding muffled and tinny with very low volume. You will want a Linux desktop environment that supports easily enabling volume overboost up to 150% for reasonable audio levels on things like meetings and movies. And as for music, do yourself a favor and use headphones or an external speaker.

Thermal management and fan noise are excellent. I don't think I've even heard the fan yet, which is no doubt attributable to the meager but highly efficient Core i3 10th gen CPU.

Finally, the design of the MSI Modern 14 is extremely commendable for its upgradeability and serviceability. There are just a few normal Philips screws that need to be removed to take off the back panel, which thankfully is just a flat cover, not a complex clamshell design. Removing the back cover of the Modern 14 allows for easily changing the NVMe drive, the WiFI card, the battery, or the RAM in the single slot, which accepts a maximum of 32GB.


## Keyboard: ⭐☆☆☆

![MSI Modern 14 keyboard](https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6452/6452969cv18d.jpg)

The keyboard has a nice tactile feel and is remarkably sturdy for heavy typists like myself, owing to the chiclet design embedded directly into the aluminum shell of the laptop. The action feels slightly damped but not mushy, with minimal but appropriate key travel. Also commendably, the Modern 14 offers a keyboard backlight, which is not a given at this price point.

The positive aspects of the keyboard unfortunately stop there. The keyboard is generally nice to type on, but it is ruined by several infuriating design aspects. First and foremost, the `Home` and `End` functions are placed at the second level of the `PageUp` and `PageDown` keys, requiring depressing the the `Fn` key. And then there is the `Fn` key itself, which is a half-sized key located on the wrong side of the keyboard next to a similarly sized `Ctrl` key. All of these feel like senseless compromises; after all, it's a normal sized 14" laptop, not a 12" or 13" netbook. So while the right side of the keyboard is designed as if there were a huge space premium to deal with, left side has a large _double-sized_ `Ctrl` key, which makes no sense. Additionally, it feels unnecessary on a laptop of this size to include full-sized arrow keys. I would much rather have a longer right `Shift` key, which is too short for my hands and typing style. As a secondary traveling machine I can accept all of these design decisions, or maybe they were random oversights, but for daily usage I would find this absolutely unacceptable. I sincerely hope that MSI complete reworks this disastrous keyboard layout in the next revision.

Apart from the layout of the keys used for traditional typing, I also lament the combining of special and multimedia keys in a second level of the `Fn` keys, forcing the user to choose between one of two equally important functions. I still think there is no better laptop keyboard layout than the Thinkpad T530, with dedicated volume and mute buttons above the `Fn` keys row for instant access even while retaining `Fn` keys. Additionally, the T530 places the keyboard and nightlight functions on the `Space` bar together with the `Fn` key, which allows for turning on the backlight -- wait for it -- _in the dark!_ Compare that with the Modern 14 and most other new laptops that place it on one of the `Fn` keys, which is extremely hard to locate while fumbling around for it in the dark. It's a small detail, but yet another pathetic sign of the paucity of common sense and effort in electronics design these days.


## Battery Life: ⭐⭐⭐☆

Battery life has been surprisingly good bordering on excellent with this particular model of the Modern 14, despite its cut-rate 39Whr battery. With the screen turned down to a still very usable 20% brightness level, it idles along at a mere 2 - 3 watts of power. With a mix of standard office tasks and web browsing and watching videos it's easy to exceed 8 hours on the battery, and well over 10 hours isn't out of the question. I still wish that MSI wouldn't skimp on the battery and provide across the board the larger 52Whr battery that some models have, which would give truly stellar battery life. But as it, I expected the worst when I saw the battery capacity label, but I have been very pleasantly surprised.


## Performance: ⭐⭐☆☆

Performance with the combination of the Intel Core i3-10110U, 8GB of RAM, and NVMe SSD is perfectly adequate for everyday usage. Although the Core i3-10110U doesn't look like much on paper, typical laptops in this price range often come with a much more limited Intel Celeron mobile processor.

Cold boot from GRUB to the openSUSE Leap 15.3 login screen takes about 10 or 11 seconds with the XFS filesystem, depending on the specific kernel version used. Duplicating an ISO file around 1.1GB in size is instantaneous. In Linux, Firefox takes less than 3 seconds to launch from cold boot, and barely 2 seconds after being cached. LibreOffice Writer takes about 2 seconds the first time and 1.5 seconds after that. I used Avidemux with the 2-pass algorithm to convert [this 928MB AVI movie](http://www.peach.themazzone.com/big_buck_bunny_1080p_surround.avi) to an MP4 with 100MB target video size and 64 kbps AAC audio, and it took 9 minutes and 28 seconds. This [1.7GB bz2 compressed archive](https://download.geofabrik.de/australia-oceania-latest.osm.bz2) took 6 minutes and 33 seconds to decompress to 21.2GB, whereas this more reasonably sized [288MB tar.gz archive](https://github.com/LibreOffice/core/archive/refs/tags/mimo-7.0.7.0.M4.tar.gz) took 17 seconds.


## Ports and Features: ⭐⭐☆☆

The MSI Modern 14 has most of the basic ports, although there are signs of senseless cost cutting. The first port toward the back of the machine on the left side is the barrel charger jack, which is medium-small in diameter. I would prefer the chunky square charge port style used in Thinkpads, which feels much more sturdy, although I've seen even smaller barrel charger ports. The next port on the left side is a full-size HDMI port, which is commendable. Next comes a USB-C port, which I find completely useless because it can't be used for charging the laptop or for external displays. Finally on the left side there is a micro SD card slot, which is good to have, but I think I would prefer the full-sized slot for use with an adapter card.

![MSI Modern 14 ports left](https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6452/6452969cv4d.jpg)
![MSI Modern 14 ports right](https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6452/6452969cv7d.jpg)

On the right side toward the back of the machine there are only two regular USB ports, and frustratingly in this particular MSI Modern 14 model configuration, both ports only support USB 2.0 speeds. This is another sign of senseless cost-cutting -- can it really be more expensive to use USB 3.0? Finally, there is a combined input/output 3.5mm audio jack, and power and charging LEDs. This is another evince of stupid design or lack thereof, as the power LED is duplicated on the top level power key, whereas the machine lacks a disk I/O notification LED, which would be much more useful. Also I would prefer at least one if not two additional full-size USB ports on the left side of the laptop, as well as an ethernet port, which is also absent (and no dongle provided).

------

## Final Score: _2.7 out of 4 stars_

In conclusion, the MSI Modern 14 (B10MW-486 / Modern14486) laptop is a highly Linux-compatible laptop with decent quality construction and components and very good battery life at a highly competitive price. Time will tell if the longevity of this laptop follows the same trends of the modern disposable computer industry. But given that even expensive modern hardware suffers from a dearth of quality, the sub-$400 MSI Modern 14 is less of a risk, and will offer an excellent Linux experience for as long as it lasts.


## ✍ Share your thoughts on this article!
**➠[https://gitlab.com/libretechtips/libretechtips.gitlab.io/issues/4](https://gitlab.com/libretechtips/libretechtips.gitlab.io/issues/4)**
