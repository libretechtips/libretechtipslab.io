---
title: "Review: DuckDuckGo (May 2014 redesign) Compared to Google, Bing, Yandex"
date: 2014-05-27
tags: ["reviews", "search", "DuckDuckGo", "Google", "Bing", "Yandex"]
---

If you are reading this article, you’ve probably already heard of DuckDuckGo. Internet users that stick with the status quo usually don’t look any farther than the first search option offered by their web browser. On the other hand, inquisitive users that depend on the Internet for statistics, comparisons, and hidden pearls of useful information are probably all too familiar with the perks and pitfalls of the current Internet search engines. DuckDuckGo was nothing more than a miniscule blip on the radar of demanding Internet searchers since its inception in 2008. However, the recent scandal of NSA monitoring resulted in a massive influx of users defecting to DuckDuckGo. And a major refresh of DuckDuckGo’s interface in May, 2014 also attracted the attention of many new users, including myself. So, how does DuckDuckGo compare to the big players in the Internet search engine field? Will users who prefer DuckDuckGo for privacy related issues find what they are searching for? And are there any advantages to using DuckDuckGo based solely on the merits of its interface and search result quality? That’s what we hope to determine in this article.

DuckDuckGo’s Search Mechanism Compared to Other Search Engines
--------------------------------------------------------------

In this comparison, we will analyze the search results, instant answers, and user interface of [Google](http://google.com), [Bing](http://bing.com), [Yandex](http://yandex.com), and [DuckDuckGo](http://duckduckgo.com). The first three are crawler-based search engines, that is to say, they run their own web crawler bots that locate and index content on the web. According to [these rankings](http://www.incapsula.com/blog/know-your-top-10-bots.html), Google, Bing, and Yandex maintain the top three most prolific web crawlers of web content in English and other languages. DuckDuckGo, on the other hand, uses a hybrid approach consisting of search results that combine [DuckDuckGo’s own web crawler](https://duckduckgo.com/duckduckbot) index with data from more than [100 additional sources](https://duck.co/help/results/sources). DuckDuckGo uses its own “intelligence layer” on top of all this to filter and re-rank the search results returned by the external sources. This means that DuckDuckGo is far more than a simple proxy access to the big search engines, and helps to establish itself as a uniquely valuable player in the crowded search engine market.

DuckDuckGo honestly admits that the effort and expense of crawling billions of web pages across the deep web is a behemoth task that is already admirably accomplished by the likes of Google and Bing. For this reason, DuckDuckGo heavily uses the search APIs provided by Bing and Yandex to access their massive indexes of web content, permitting DuckDuckGo to focus more on its own search algorithms, interface, and overall quality of results. One major purported advantage of DuckDuckGo’s “intelligence layer” is the filtering of irrelevant link spam sites and parked domains that often clutter the results of the most popular search engines.

Compared to the other three search engines, DuckDuckGo lays claim to the unique advantage of not tracking its users in any way. At the same time, it provides its users with access to the wealth of data indexed in Bing and Yandex without submitting users to these site’s tracking mechanisms. For this reason, many privacy conscious searchers simply insist on DuckDuckGo regardless of the quality of its results. Other users don’t care as much about privacy as they do about relevancy and variety of the search results. An additional side effect of DuckDuckGo’s lack of user tracking, be it positive or negative, is that all users across the globe will see the same search results for a given query. Google, Bing, and Yandex, on the other hand, personalize the results of each user’s search depending on many factors like geographic location and prior search history. So how well does it all work in practice?

Criterion \#1: Relevancy of Results
-----------------------------------

**(Weight: 50%)**

### Example Search \#1: “canon mp250 reset”

This first example search is an attempt to find information about a fairly obscure trick needed to reset my Canon MP250 printer after refilling the cartridge. I will perform all searches in a private browser window to minimize the amount of personalization that Google, Bing, and Yandex will perform on my search results.

#### Google

**Grade: ★★☆☆**

[![](/20140527-search/01_google.png)](/20140527-search/01_google.png)

*(Click to view the full size image)*

For obvious reasons, Google’s seems to give priority to Youtube results for this search. I generally dislike Youtube videos in my search results, because if I am using a search engine I am generally looking for quick written information, not videos. I can skim through a written article and retrieve necessary information much faster than sitting through a rambling, unprofessional Youtube video. If I want videos, I will search directly on Youtube. Apart from Youtube, there were some links to official Canon manuals and articles, which looks promising but in reality doesn’t give me the solution I’m looking for. There is also a junky Yahoo Answers post that doesn’t help. In general, Google seems to prefer corporate or large established mega-sites, which isn’t very helpful in this case as my query has to do with an obscure trick that is not officially published. The 7th result, although a rather nefarious site, does finally give the solution.

#### Bing

**Grade: ★★☆☆**

[![](/20140527-search/01_bing.png)](/20140527-search/01_bing.png)

*(Click to view the full size image)*

Bing seems to have problems recognizing languages. Even though I specified English results only, the first two results were again Youtube videos, but this time in Spanish. Additionally, if it’s going to show Youtube in the results I would appreciate a preview like Google gives. Apart from this, Bing gave me fairly varied results, including a question on a help forum, and a few articles about the subject. Overall, the results were just slightly more relevant than Google.

#### Yandex

**Grade: ★★☆☆**

[![](/20140527-search/01_yandex.png)](/20140527-search/01_yandex.png)

*(Click to view the full size image)*

Yandex doesn’t prefer Youtube videos, which is good. It also gave me only English results, even though the most popular articles and bulk of the information about this printer is in Spanish. The first two results were articles about my specific printer model. In the other results Yandex seems to give slightly higher priority to file sharing and download sites, which I don’t like. I would never just download some random file that a search engine suggests on a topic unless an article specifically linked to it.

#### DuckDuckGo

**Grade: ★★★☆**

[![](/20140527-search/01_ddg.png)](/20140527-search/01_ddg.png)

*(Click to view the full size image)*

DuckDuckGo basically provides many of the same top results as Google, Bing and Yandex, albeit in a different order. It does not give high priority to Youtube or file download sites. The results were a decent mix of specific articles about my printer model and forum posts. Some of the first six or seven results do offer help on this topic, and seem to be based more on relevancy to my query and not so much on the size or popularity of the site. Overall, the results are slightly more helpful than the big three.

### Example Search \#2: “obnam mount”

This search is intended to give instructions on how to mount a backup created by the “Obnam” backup utility onto a Linux filesystem. Again, it a fairly obscure topic, and is further complicated by the obvious similarity of the software title “obnam” to the name “Obama”.

#### Google

**Grade: ★★★★**

[![](/20140527-search/02_google.png)](/20140527-search/02_google.png)

*(Click to view the full size image)*

Google aced this query. The first link is for a blog post explaining exactly how to mount backups with Obnam, and the next two results are to the official manual pages for Obnam.

#### Bing

**Grade: ★★★☆**

[![](/20140527-search/02_bing.png)](/20140527-search/02_bing.png)

*(Click to view the full size image)*

Bing’s results were also pretty good for this query. The first result is for the same very helpful blog post. The next couple of results aren’t as helpful, consisting of news and bug reports about the Obnam mount feature. But links to the manual pages are soon to follow within the first six or seven results.

#### Yandex

**Grade: ★★☆☆**

[![](/20140527-search/02_yandex.png)](/20140527-search/02_yandex.png)

*(Click to view the full size image)*

The Yandex results weren’t all that great for this query. The first result is about bugs for the Obnam mount feature. The next result is the same very relevant blog post that Google and Bing located, which is good to see. But the rest of the results are more noise and less relevant. The official Obnam manual is not featured prominently in the top results.

#### DuckDuckGo

**Grade: ☆☆☆☆**

[![](/20140527-search/02_ddg.png)](/20140527-search/02_ddg.png)

*(Click to view the full size image)*

Oops. DuckDuckGo fell flat on its face with this query. I appreciate that DuckDuckGo is trying to correct what it perceives as a spelling mistake. And admittedly this looks like an almost positive attempt to search for “Mount Obama”. But unfortunately it’s completely mistaken. And even more strangely, although DuckDuckGo seems to be going on the assumption that I’m looking for information about Mount Obama, it still mixes in the occasional page about obnam mounting, albeit infrequently. So DuckDuckGo seems to be totally confused about this one. It really should not automatically correct my search terms without asking me “Did you mean…?” and *me having accepted that suggestion first*. And if it does automatically correct some words, it shouldn’t mix in the other terms that it determined to be mistaken. What would be ideal for this query is a disambiguation page, with the default option being what I typed, and a secondary selectable option for Mount Obama in case I meant that. That being said, I was able to set it straight by putting the term “obnam” in quotes. That produced decent (but not great) results, although some users probably wouldn’t think to do this and would undoubtedly resort to a different search engine for this query.

### Example Search \#3: “vultr review”

I decided to look for reviews of a fairly new and unknown virtual private server provider called “vultr”. This is a good test of a search engine’s prowess for finding unbiased reviews when we most need them, that is to say, when the product or service is still new or unproven.

#### Google

**Grade: ★★★★**

[![](/20140527-search/03_google.png)](/20140527-search/03_google.png)

*(Click to view the full size image)*

Google’s results for this search were excellent. All of them were relevant and well assorted, consisting of links to reviews in forums dedicated to web host reviews, and personal blogs.

#### Bing

**Grade: ★★☆☆**

[![](/20140527-search/03_bing.png)](/20140527-search/03_bing.png)

*(Click to view the full size image)*

Bing yielded noticeably poorer results for this search. Strangely, several of the links pointed to the top level domain of a site containing reviews for Vultr, and not to the actual page that contains the review. It’s as if Bing said: “We got you this far. Now you figure out where it’s at from here.” Several of the other top links pointed to sites that no longer worked.

#### Yandex

**Grade: ★★★☆**

[![](/20140527-search/03_yandex.png)](/20140527-search/03_yandex.png)

*(Click to view the full size image)*

Yandex’s results weren’t bad on this search. It returned several posts in web hosting review forums as well as a blog. A little bit farther down it started to get off track, with links to IP address lookup for Vultr, and some image results in Chinese.

#### DuckDuckGo

**Grade: ★☆☆☆**

[![](/20140527-search/03_ddg.png)](/20140527-search/03_ddg.png)

*(Click to view the full size image)*

DuckDuckGo again bungled the search results with a misguided attempt at guessing what I’m searching for and correcting me without my approval. On this search, it suggested: “Did you mean ***vulture review***?” It’s a reasonable question, but I never accepted the correction. Even so, DuckDuckGo went ahead and gave me results that mostly contained the term “vulture”, and even more erroneously, “vault”, instead of “vultr”. Again, I was able to rectify it by putting the term “vultr” in quotes, which actually led to pretty relevant results. But DuckDuckGo is really shooting itself in the foot with its automatic correction and disambiguation.

### Example Search \#4: “newfrog.com rating”

The Internet is obviously an extremely important vehicle for buying and selling. But in order to avoid a bad experience, a good search engine is needed to research the reputation of an online retailer before purchasing. In this example, I searched for ratings on a retailer that I know to be shady.

#### Google

**Grade: ★★★★**

[![](/20140527-search/04_google.png)](/20140527-search/04_google.png)

*(Click to view the full size image)*

Google had no trouble locating relevant reviews for the retailer, all of which clearly indicate warning signs about this shady retailer. The results were varied, containing ratings by large website reputation sites, online retail reputation sites, and smaller forums.

#### Bing

**Grade: ★★☆☆**

[![](/20140527-search/04_bing.png)](/20140527-search/04_bing.png)

*(Click to view the full size image)*

Bing’s results were considerably less relevant. While there were a few of the same helpful sites that Google turned up, Bing also highly ranked several Youtube videos about products that are apparently sold by the retailer in question, together with a link to the top level domain of a different shady online retailer.

#### Yandex

**Grade: ★☆☆☆**

[![](/20140527-search/04_yandex.png)](/20140527-search/04_yandex.png)

*(Click to view the full size image)*

The top results in Yandex were even less helpful. Two of them were pages created by the retailer in question, and the others were mainly about the website’s value and traffic statistics. Only one of the top results gave relevant reviews about the company’s business practices.

#### DuckDuckGo

**Grade: ★★★☆**

[![](/20140527-search/04_ddg.png)](/20140527-search/04_ddg.png)

*(Click to view the full size image)*

DuckDuckGo’s top results were quite helpful, with links to several of the most helpful results that Google found. However, scrolling farther down gives yet another example of DuckDuckGo’s tendency toward second-guessing the user and changing the search query without the user’s approval. Although the top results were good, some of the lower ranked results were rather amusing: “The Princess and the Frog Reviews & Ratings – IMDb”, “New Green Frog in Fort Walton Beach”, and “PetSmart – Green Tree Frog customer reviews”.

### Example Search \#5: “top mountain bikes”

This search is intended to test the search engine’s ability to read into the user’s meaning and give relevant results. Although I am generally critical of search engines that rewrite the user’s query automatically, in certain cases it is helpful when the search engine includes very common synonyms of some search terms. In this case, a user searching for “top mountain bikes” probably wants to find amateur and professional opinions about the “*best* mountain bikes”. Furthermore, the results should be unbiased, and should include both professional and owner reviews.

#### Google

**Grade: ★★★★**

[![](/20140527-search/05_google.png)](/20140527-search/05_google.png)

*(Click to view the full size image)*

Google produced useful results for this search. Most of its results were about the “best” bikes instead of the “top” bikes, which is a logical and helpful inclusion. The results varied from articles about the best bikes at sites about mountain biking, to top consumer ratings for mountain bikes sold on Amazon. Most of the links took me directly to pages with rankings of the best bikes.

#### Bing

**Grade: ★★★☆**

[![](/20140527-search/05_bing.png)](/20140527-search/05_bing.png)

*(Click to view the full size image)*

Bing’s results were also helpful. It also inferred that I wanted to know about the “best” bikes and not just the “top” bikes. However, Bing loses a point for including several highly ranked links to sites of specific bike manufacturers that are obviously neither objective nor helpful for this sort of search.

#### Yandex

**Grade: ★★☆☆**

[![](/20140527-search/05_yandex.png)](/20140527-search/05_yandex.png)

*(Click to view the full size image)*

Yandex’s results left a lot to be desired for this query. A few of the top results were relevant, but the others tended toward information about the top mountain biking locations instead of the bikes themselves. Like Bing, Yandex gave higher priority to the sites of some bike manufacturers, which isn’t particularly helpful for this sort of search. Unlike the first two engines, Yandex seemed to give much higher priority to sites using the word “top” and not so much to sites that substituted it with the word “best”.

#### DuckDuckGo

**Grade: ★★★☆**

[![](/20140527-search/05_ddg.png)](/20140527-search/05_ddg.png)

*(Click to view the full size image)*

DuckDuckGo’s results were much better than Yandex’s and marginally better than Bing’s. There were several highly ranked links to rating pages of mountain bikes on general consumer sites, together with some rankings by sites specific to mountain biking, as well as Amazon’s rating pages. But DuckDuckGo lost a point for including a highly ranked result to a specific bike manufacturer’s site, as well as a useless link to a page of eBay listings.

### Example Search \#6: “biggest web crawlers”

This is another query that requires the search engine to make inferences about what is meant by “biggest”. I made this search to get information for the preparation of this article. Apparently there isn’t a lot of information online about the biggest web crawlers in terms of indexed pages. None of the search engines returned an abundance of relevant results for this particular search, and some of them were considerably worse than others.

#### Google

**Grade: ★★★☆**

[![](/20140527-search/06_google.png)](/20140527-search/06_google.png)

*(Click to view the full size image)*

Google gets the highest mark of the four search engines because it was the only one that found the information I was looking for. The very first result gives a clear, relevant answer with statistics. It also included a link to the Wikipedia page about web crawlers, which is a useful read. The other results were unhelpful, but at least they did have something to do with crawling and indexing web content.

#### Bing

**Grade: ★★☆☆**

[![](/20140527-search/06_bing.png)](/20140527-search/06_bing.png)

*(Click to view the full size image)*

Bing’s results were quite poor. With the exception of the Wikipedia entry, everything else focused on options for personal web crawlers, not the ones operated by the search engines. But at least the results were relevant to some sort of web crawling.

#### Yandex

**Grade: ★★☆☆**

[![](/20140527-search/06_yandex.png)](/20140527-search/06_yandex.png)

*(Click to view the full size image)*

Yandex’s results were even less helpful than Bing’s, and seemed to focus mainly on domains that contain the word “crawl” in their name.

#### DuckDuckGo

**Grade: ★☆☆☆**

[![](/20140527-search/06_ddg.png)](/20140527-search/06_ddg.png)

*(Click to view the full size image)*

With the exception of the Wikipedia entry, DuckDuckGo’s results were worthless for this query. Worse still, many of the results had absolutely nothing to do with crawling the web. In the mix were links about internet pioneers, life insurance, and nightcrawlers. DuckDuckGo has definitely established a pattern of using strange fuzzy logic, producing weird results that are totally irrelevant to the intended query.

### Example Search \#7: “saturated fat healthy”

An important aspect of a good Internet search engine is being unbiased and fair. This search is intended to gauge the search engines’ level of neutrality when returning results on controversial topics. This query should produce links to conflicting opinions and studies, some claiming that saturated fat is healthy and others stating that it is harmful. It is vital that the search engine return both sides of the debate. The reader must form an opinion on any given topic, and the search engine should not attempt to do so for him.

#### Google

**Grade: ★★★☆**

[![](/20140527-search/07_google.png)](/20140527-search/07_google.png)

*(Click to view the full size image)*

Google’s results were quite varied. Of the top ten results, six were in favor of saturated fat, and four recommended against it. All of the results were relevant to the topic. Google only lost a point for not including a neutral reference like Wikipedia.

#### Bing

**Grade: ★★★★**

[![](/20140527-search/07_bing.png)](/20140527-search/07_bing.png)

*(Click to view the full size image)*

Bing’s results were all relevant, and presented both sides of the debate. Of the top ten results, three were in favor of saturated fat, one was a neutral Wikipedia article, and 6 articles recommended against saturated fat.

#### Yandex

**Grade: ★★★☆**

[![](/20140527-search/07_yandex.png)](/20140527-search/07_yandex.png)

*(Click to view the full size image)*

Yandex also presented both sides of the debate, albeit slightly more slanted in favor of saturated fat. Of the top ten results, seven were in favor of saturated fat and three were against it. Like Google, it loses a point for failing to give a neutral reference article.

#### DuckDuckGo

**Grade: ★★★★**

[![](/20140527-search/07_ddg.png)](/20140527-search/07_ddg.png)

*(Click to view the full size image)*

DuckDuckGo’s results were excellent, providing relevant and varied articles presenting both sides of the issue, together with a neutral Wikipedia entry. Of the top ten results, six were in favor of saturated fat, the Wikipedia article was neutral, and three recommended against saturated fat.

### Example Search \#8: “anti-inflammatory bacteria”

This next query is a test of the search engines’ ability to find relevant information about a fairly obscure, specialized, technical topic. Ideally, results should be a mix of technical articles and studies, together with some articles that explain the topic in layman’s terms.

#### Google

**Grade: ★★★☆**

[![](/20140527-search/08_google.png)](/20140527-search/08_google.png)

*(Click to view the full size image)*

Google turned up the biggest variety of technical research papers on this topic, together with an easier to understand article, and a Wikipedia entry. The results were impressively comprehensive, but I would have liked to have seen a little bit more information oriented toward normal readers. Since most of the technical research papers only show an abstract and require paid access, they are less useful to normal readers.

#### Bing

**Grade: ★★☆☆**

[![](/20140527-search/08_bing.png)](/20140527-search/08_bing.png)

*(Click to view the full size image)*

Bing’s results also seemed to give a heavy focus to the pay-to-access technical research studies. One result was a bad link, and a few results were less relevant to the topic.

#### Yandex

**Grade: ★☆☆☆**

[![](/20140527-search/08_yandex.png)](/20140527-search/08_yandex.png)

*(Click to view the full size image)*

Yandex’s results were fairly unhelpful. Seven of the top ten results followed a different tangent of the topic or were otherwise unhelpful. There were just a few technical and general references about bacteria that exhibit an anti-inflammatory effect.

#### DuckDuckGo

**Grade: ★★★☆**

[![](/20140527-search/08_ddg.png)](/20140527-search/08_ddg.png)

*(Click to view the full size image)*

DuckDuckGo returned a fairly comprehensive and varied selection of relevant links, including both technical papers and simple articles. Just a few of the top ten links that were less relevant to the topic.

### Example Search \#9: “whole wheat tortillas”

What would the Internet be like without recipes? Indeed, one of the primary uses of search engines since their inception has been for recipe searches. I intentionally left out the word “recipes” from this query to test the level of bias, if any, that the search engines might have toward commercial sites that are trying to sell a product.

#### Google

**Grade: ★★★☆**

[![](/20140527-search/09_google.png)](/20140527-search/09_google.png)

*(Click to view the full size image)*

Google returned a good selection of relevant recipes with helpful thumbnails of the prepared tortillas. However, two of the top ten results were not recipes, but rather companies that sell prepared whole wheat tortillas.

#### Bing

**Grade: ★★★☆**

[![](/20140527-search/09_bing.png)](/20140527-search/09_bing.png)

*(Click to view the full size image)*

Bing’s results were also very relevant, and it only returned one high ranking result to a company that sells tortillas. It would be very nice to see thumbnail images of the prepared recipes, however.

#### Yandex

**Grade: ★★★☆**

[![](/20140527-search/09_yandex.png)](/20140527-search/09_yandex.png)

*(Click to view the full size image)*

Yandex’s results were also good. Only one of the top ten results was a commercial company selling tortillas. Again, thumbnails were lacking.

#### DuckDuckGo

**Grade: ★★★☆**

[![](/20140527-search/09_ddg_1.png)](/20140527-search/09_ddg_1.png) [![](/20140527-search/09_ddg_2.png)](/20140527-search/09_ddg_2.png)

*(Click to view the full size images)*

DuckDuckGo initially offered me a horizontal row of product options for whole wheat flour tortillas for purchase, complete with thumbnails. However, there was also a disambiguation option of switching the horizontal thumbnail results to recipes. Although many of these were recipes for complete dishes calling for whole wheat flour tortillas, there were also a few recipes just for whole wheat tortillas, complete with a thumbnail, which was very helpful. Apart from the horizontal thumbnail results, there were also some more regular search results. Several of these were pages selling tortillas, together with another page about nutritional information for whole wheat tortillas. But all in all, the results were varied and helpful.

### Example Search \#10: “patagonia”

This final search query is meant to test the search engines’ ability to disambiguate, that is to say, determine multiple meanings for a search term and give the user the option to choose which (s)he is looking for. This is an important feature that helps to give some semblance of order to the confusing plethora of topics with almost the same keywords on the Internet. In this case, I want to know about the region called Patagonia in South America. However, there is also a brand of clothing, a movie, and another place in Arizona, all named Patagonia.

#### Google

**Grade: ★★☆☆**

[![](/20140527-search/10_google.png)](/20140527-search/10_google.png)

*(Click to view the full size image)*

Google was convinced that I mainly wanted to know about the Patagonia clothing company. It used a lot of screen real estate to show me an expanded site index of the Patagonia clothing company, together with an info box about the Patagonia and related brands. The Wikipedia articles about both the place in South America and the company also appeared. There was also a highly ranked link to a worthless Twitter account for the Patagonia clothing company. The only saving grace was the fact that Google did offer disambiguation scattered around the page. There was a “See results about” box with thumbnails for the two places called Patagonia. Also at the bottom of the page there was a section for “Searches related to ***patagonia***”. But the overall presentation of this page was overwhelming and cluttered in appearance, with a heavy commercial focus.

#### Bing

**Grade: ★★☆☆**

[![](/20140527-search/10_bing.png)](/20140527-search/10_bing.png)

*(Click to view the full size image)*

Bing also showed a strong preference for commercial results with this search. It mainly presented results for companies and social networks, a Youtube video, and sponsored ads about hotels in Patagonia, all of which were unhelpful. There was one link to the Wikipedia article about the place in South America called Patagonia. And like Google, Bing also offered two separate sections of suggestions for related searches about “patagonia”, one at the side and another at the bottom.

#### Yandex

**Grade: ☆☆☆☆**

[![](/20140527-search/10_yandex.png)](/20140527-search/10_yandex.png)

*(Click to view the full size image)*

Yandex failed at this search due to its complete lack of disambiguation. Its results were a mixed bag of links about the company, a Youtube video, social networks, and only a few links to the place in South America.

#### DuckDuckGo

**Grade: ★★★☆**

[![](/20140527-search/10_ddg.png)](/20140527-search/10_ddg.png)

*(Click to view the full size image)*

DuckDuckGo was the most helpful at disambiguation for this search term. The new site redesign clearly paid off in the area of recognizing and clearly presenting different meanings of search terms with a prominent and clean presentation of the options. It again uses a horizontal row of illustrated boxes with a thumbnail and text description of different possible meanings. These are extremely helpful and easy to understand. Clicking on any of them with refine the search to focus on the desired meaning. I would have given DuckDuckGo a full 4-star rating for this fantastic feature; however, it loses a point because the even after clicking on the box for Patagonia in South America, most of the results are still about products with Patagonia in their name, together with some worthless social network links. However, at least the description of the Patagonia region told me where it is located, so I can add a few additional terms like “Argentina” or “Chile” to get much more relevant results.

### Criterion \#2: Instant Answers

**(Weight: 25%)**

Internet users are increasingly turning to the search engine as a primary source of quick answers, statistics, and calculations. Ideally, a good search engine should return the desired facts or the result of a requested calculation immediately, without the need of clicking on any additional links or visiting third party websites. We will now proceed to examine the quality and availability of instant answers, if applicable, on Google, Bing, Yandex, and DuckDuckGo, using a selection of common questions and calculations.

#### Google

**Grade: ★★★☆**

[![](/20140527-search/instant_google_1.png)](/20140527-search/instant_google_1.png) [![](/20140527-search/instant_google_2.png)](/20140527-search/instant_google_2.png)

*(Click to view the full size images)*

1.  **Currency conversion:** *10 euros in yen* — Perfect. Gives instant conversion with edit boxes to change the sources and target values. Also shows the exchange history.
2.  **Complex currency/weight conversion:** *250 mexican pesos/kg in usd/lb* — Very good. Confirms the full name of the source and target currency in case abbreviations were used.
3.  **Value of a constant:** *gravitational constant* — Perfect. Exponents are formatted in superscript like they should be.
4.  **Fuel economy conversion:** *5.6 L/100km in mpg* — Good, but doesn’t confirm that *L* means *Liters*.
5.  **Tracking numbers:** *LJ\*\*\*\*\*\*\*\*\*US* , *RT\*\*\*\*\*\*\*\*\*HK* — Hit and miss. Appears to always provide a direct link to the US Post Office for the provided tracking number, even in the case of Hong Kong Post tracking number, which was never registered in the USPS system.
6.  **Weather:** *weather in istanbul* — Perfect. Provides the current temperature, current precipitation probability, humidity, wind speed, temperature/precipitation/wind history graph, and an 8-day forecast.
7.  **Time:** *time in shanghai* — Perfect.
8.  **Define a term:** *definition indefatigable* — Perfect. Gives a good definition with synonyms and audio pronunciation in a quick results box at the top of the page. The expansion arrow shows a staggering amount of information about etymology, translation, and usage of the word over time.
9.  **Translate a term:** *arm in german* , *foot in german* — Hit and miss. Offers a quick translation box for most terms between two different languages. But strangely, it couldn’t find a translation for *arm* in German.
10. **Quick facts:** *capital of canada* — Perfect. A box appears at the top informing me that Ottawa is the capital, complete with a picture, map, and additional facts and statistics in the expandable part of the box.
11. **Local shopping and dining:** *thai food in bloomington in* — Pretty good. I would prefer a separate section at the top for this sort of information, but it does offer a list of local Thai restaurants at the specified location, together with ratings and address/telephone information.

#### Bing

**Grade: ☆☆☆☆**

-   Sorry, no instant results at all on Bing.

#### Yandex

**Grade: ☆☆☆☆**

-   Sorry, no instant results at all on Yandex either.

#### DuckDuckGo

**Grade: ★★★☆**

[![](/20140527-search/instant_ddg_1.png)](/20140527-search/instant_ddg_1.png) [![](/20140527-search/instant_ddg_2.png)](/20140527-search/instant_ddg_2.png)

*(Click to view the full size images)*

1.  **Currency conversion:** *10 euros in yen* — Almost perfect. Gives instant conversion, but no edit boxes to change the sources and target values. No exchange history. It does confirm that these are Japanese Yen we are talking about. Inside the instant answer box it does provide a “More at WolframAlpha” link, which provides all the information that Google does plus a whole lot more. It’s just that it takes one more click.
2.  **Complex currency/weight conversion:** *250 mexican pesos / kg in usd/lb* — Perfect. Confirms the full name of the source and target currency and weights in case abbreviations were used.
3.  **Value of a constant:** *gravitational constant* — An answer is provided, but the number is harder to read due to the lack of formatting for exponents. It just looks like this: `6.67300 10^-11 m^3 kg^-1 s^-2` Unfortunately, there no link to Wolfram Alpha, which provides beautifully formatted constants in different measurements systems.
4.  **Fuel economy conversion:** *5.6 L/100km in mpg* — Perfect. Confirms that *L* means *Liters*.
5.  **Tracking numbers:** *LJ\*\*\*\*\*\*\*\*\*US* , *RT\*\*\*\*\*\*\*\*\*HK* — Hit and miss. For the USPS tracking number it provides a direct link to a tracking website called “LaserShip.com”, which was unable to find the tracking number. However, it does appear that it tries to give an appropriate link for whatever postal system issued the tracking number. In the case of the Hong Kong Post number, it provided me a direct link to the Hong Kong Post tracking page for the provided tracking number.
6.  **Weather:** *weather in istanbul* — Quite good. It specifically identified the region of Istanbul being reported, namely “Sisly”. Provides the current temperature, current precipitation, current wind speed and direction, and an 7-day forecast. It provides a link to “Forecast.io” for many more details and even animated weather maps.
7.  **Time:** *time in shanghai* — Perfect.
8.  **Define a term:** *definition indefatigable* — Good. Notice that the keyword “definition” must be used. I would prefer that it also accept the keyword “define”, which seems more natural to me. Provides a basic pronunciation guide and definition. Much more information can be found at the link to the word on Wordnick.com, but unfortunately that requires visiting a third party site.
9.  **Translate a term:** *arm in german* , *foot in german* — DuckDuckGo can not currently give instant translation results. However, using a “bang” operator, it will take you directly to the Google Translate page and fills in the source word. For example: `!tr arm`
10. **Quick facts:** *capital of canada* — Minimal, only returns “Ottawa, Ontario, Canada”. However, there is a link to the incredibly detailed information about Ottawa at Wolfram Alpha.
11. **Local shopping and dining:** *thai food in bloomington in* — Excellent. Provides a horizontal row of local restaurants at the specified location with a thumbnail and star rating for each one. Upon clicking one, the box displays the address and phone number, and a large map appears to pinpoint the location.

### Criterion \#3: Interface

**(Weight: 25%)**

Considering the fact that the Internet search engine is one of the most frequently used tools on most computers, the default interface should be pleasant and efficient. Options to customize the look and behavior of the search engine are a nice bonus.

#### Google

**Grade: ★★★☆**

[![](/20140527-search/interface_google.png)](/20140527-search/interface_google.png)

*(Click to view the full size image)*

Google’s default interface is sensible and attractive. Google usually has a knack for looking modern and clean, and their flagship search engine is no exception. By default Google has auto-completion / guessing of search terms, which is usually uncannily accurate. They can be disabled if desired. I also like how it shows a site menu for some sites, and multiple relevant results from the same domain are usually grouped underneath the main domain. This feature is hit and miss, however, and sometimes it puts some links from the same domain as children of one result while scattering other links to the same domain elsewhere as top-level results. Google usually offers helpful thumbnails generated from the content of many articles. Google offers a very useful set of controls to filter search results according to certain criteria such as date and reading level. Other customization options are limited on Google. The number of text ads and sponsored search results is sometimes overwhelming for certain queries, as might be expected of an advertising company like Google.

#### Bing

**Grade: ★★☆☆**

[![](/20140527-search/interface_bing.png)](/20140527-search/interface_bing.png)

*(Click to view the full size image)*

I personally hate Bing’s home page with its changing image background. The search results page is reasonably clean, but its layout and color scheme looks very dated, as if it was trying to copy Google’s theme from ten years ago. It puts a helpful list of related searches at the side and the bottom of most search result pages. Its customization options are quite limited. Surprisingly, Bing is the only search engine in this comparison that does not offer auto-completion of search terms. It also does not offer filtering of results by date. Bing has a few text ads for certain queries, but they are unobtrusive.

#### Yandex

**Grade: ★★☆☆**

[![](/20140527-search/interface_yandex.png)](/20140527-search/interface_yandex.png)

*(Click to view the full size image)*

Yandex also has an annoying home page, with its donut-shaped water image that pans along with the mouse pointer. The search results page looks dated. I’m not sure if I like the placement of the site icons on the right side of the search results. It doesn’t offer many customization options, but it does offer a bar of search options to enable/disable the security filter and filter by date, language, or file format. Yandex does offer suggestions for auto-completion of search terms. It does not seem to have any advertising, which is nice.

#### DuckDuckGo

**Grade: ★★★☆**

[![](/20140527-search/interface_ddg.png)](/20140527-search/interface_ddg.png)

*(Click to view the full size image)*

In my opinion, the new DuckDuckGo design is both beautiful, modern, and extremely functional. It is also the star in customization, offering far, far more options that all of the other three search engines combined. Auto-completion of terms is enabled by default and works well, but it can be disabled. By default it displays the favicon of the domain that each link points to, and it can also optionally display WOT (Web Of Trust) third party ratings for some sites. Another feature that I personally love is the infinite scrolling and automatic loading of additional results. After scrolling to the bottom of approximately the first 25 results, it uses an AJAX command to load another 25 or so, and continues to do so as long as more results are available. This behavior can be disabled in favor of a traditional pager, if desired. DuckDuckGo offer various color schemes and site designs, and the colors and fonts can even be personalized. It even offers keyboard navigation of the site for those who prefer it. DuckDuckGo has a few relevant and unobtrusive text ads by default on some result pages, but they generously allow the ads to be turned off in the settings. As mentioned previously, the new disambiguation and instant answers box with its thumbnails and summaries of Wikipedia results for myriads of terms is extremely information-dense and pleasing to the eye. DuckDuckGo would have received a resounding 4-star rating in this category, but I docked a point due to a few questionable interface decisions. First and foremost, DuckDuckGo displays the URL at the bottom of each result. All of the other search engines in this comparison display the URL directly below the page title, permitting me to quickly discern if the link looks spammy or irrelevant. The URL is by far one of the clearest distinguishing factors that I take into account before opening any search result link, so DuckDuckGo’s placement of the URL below the summary makes my eyes jump back and forth from the page title to its URL at the bottom and then back up to the summary sandwiched in the middle. Unfortunately this is one of the few things that can’t be customized as of yet on DuckDuckGo, and I strongly feel that it should be changed. Additionally, DuckDuckGo only displays the top level domain name for each search result by default, with the complete URL visible on mouseover. Fortunately complete URLs can be displayed by default with a setting change, but I feel that should be the default setting. Finally, DuckDuckGo does group results together by top level domain like Google does. However, it does not show the additional results for the same domain as child links. Instead, it requires a “More from example.com” link to be clicked to show additional potentially relevant results from a given domain, which feels very inefficient. In summary, I feel that DuckDuckGo has catapulted itself solidly into Google’s territory with the recent site redesign, and indeed surpasses Google in many aspects of its beautiful and efficient new interface. Still, it still has some improving to do.

Final Rankings
--------------

All four of the tested search engines proved to be surprisingly capable and replete with extensive indexes of accessible search results. At the same time, their search results for certain queries were often very different. If anything can be learned from this comparison, it could be said that a thorough comparative search of certain topics is still incomplete without consulting several of the search engines discussed in this article. But most Internet users need a go-to search engine that they turn to by default for most everyday searches and quick facts and figures. Who came out on top in this comparison? The following rankings are based on a weighted average— The ten search tests have been averaged for each search engine into one figure with a weight of 50% toward the final grade. Then, each search engine’s instant results grade and interface grade have been figured into the final score at 25% each.

#### First place: Google

**Weighted Average:** **3.1** out of **4** stars

With its uncannily accurate interpretation of search results, incomparably massive index of web content, excellent instant answers, and clean interface, Google is still the one to beat.

#### Second place: DuckDuckGo

**Weighted Average:** **2.7** out of **4** stars

DuckDuckGo has suddenly become a major challenger to Google with its mashup of many data sources, excellent instant answers, and attractive new interface. It was only harmed by its tendency toward automatically correcting search terms and replacing them with other ones contrived though flawed fuzzy logic.

#### Third place: Bing

**Weighted Average:** **1.8** out of **4** stars

Considering the amount of money and resources being thrown at it, Bing’s showing here in a distant third place is really quite pitiful. Bing is an unfortunate case of mediocre search results combined with a complete lack of instant answers and an ugly interface.

#### Last place: Yandex

**Weighted Average:** **1.5** out of **4** stars

Yandex isn’t really a player at this point. But keep an eye on it, as Yandex is a big company and its search engine is still growing and improving.

Concluding Thoughts
-------------------

The competition between Google and DuckDuckGo proved to be surprisingly fierce. In many respects the tiny DuckDuckGo holds its own against the giant that is Google, and even more so if the user is willing to slightly manipulate the search query to work around DuckDuckGo’s temperamental intelligence layer. So it is heartening to see that DuckDuckGo is a viable alternative to Google by its own merits. But the elephant in the room here is Google’s extensive tracking of user data. For that reason many users will staunchly avoid it on moral grounds, and for them the natural recourse is DuckDuckGo. Fortunately for them, it’s a really great choice. In my case, privacy is not a primary concern. But having a top-notch search engine is. That’s why I set DuckDuckGo as my browser’s default search engine, and here’s hoping it stays there for a long time.
