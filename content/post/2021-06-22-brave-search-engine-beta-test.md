---
title: "Detailed test of Brave Search (beta)"
date: 2021-06-22
tags: ["reviews", "search", "Brave", "Google", "DuckDuckGo"]
---

Since my last in-depth [comparison review of search engines in 2020](https://libretechtips.gitlab.io/detailed-tests-of-search-engines-google-startpage-bing-duckduckgo-metager-ecosia-swisscows-searx-qwant-yandex-and-mojeek), there are two new and very promising options: [Whoogle](https://github.com/benbusby/whoogle-search), designed as an anonymous proxy to Google, and [Brave Search](https://search.brave.com), which is a new and independent search engine that we'll review in this article.

I'm genuinely excited about Brave Search. As mentioned in previous articles on this site, I am a pragmatist, not a privacy pundit. However, I have become increasingly frustrated with Google's search performance, and its commercial focus is increasingly imposing and overt. I can't shake the feeling that Google is wielding its massive trove of personal information and its artificial intelligence prowess with a view to taking advantage of me instead of offering me better service. Unfortunately, the alternatives up until now have been sadly lacking, and Google continues to be the defacto search engine by a massive margin. Even a company as large as Microsoft with its deep pockets and massive human and technical resources has failed to turn its Bing product into a compelling alternative search engine. And unfortunately, Bing's limited search index and mediocre query logic serve as the basis for most other alternative search engines, including the darling of privacy zealots, DuckDuckGo. My previous tests have shown that DuckDuckGo and most other smaller search engines simply regurgitate Bing's poor search results, offering mainly privacy and/or anonymity as their main selling point. Simply put, creating a good search engine is extremely hard. So Google continues to rein supreme, not by virtue of its excellence, but rather due to the lack of viable alternatives.


# Brave Search

![Rome wasn't built in a day](/20210622-brave/01-beta-rome.png)

Enter [Brave Search](https://search.brave.com). Brave has been testing its search engine with a selected group of early adopters, and just today it opened the service to the general public with a beta label. Brave Search purports to be "[the first independent privacy search/browser alternative to big tech](https://brave.com/brave-search-beta/)". The offering appears to be targeted not only at the obvious culprit Google, but also at the likes of DuckDuckGo and other privacy-focused search engines that are dependent on Bing or Google APIs for the core of their service. This year, the Brave company [acquired the independent Tailcat search engine](https://brave.com/brave-search/) (formerly operated by Cliqz), which is now positioned as the core of the Brave Search functionality. At the same time, Brave appears to be showing its pragmatism by admitting that Rome wasn't built in a day, and therefore its search results are [supplemented as needed with a mix of results from Google and/or Bing](https://search.brave.com/help/independence):

> The search results independence metric is the percentage of searches, across all result types (web, news, images, videos), that are served directly out of Brave's independent index of the web. The remaining percentage come from anonymous API calls to third parties.
> [...]
> By results we mean URLs, text snippets, rich-headers, maps, infoboxes, images, etc. [...] fetched anonymously from third parties —namely Google and Bing— and mixed in your browser to maintain privacy.

This support from more mature search indexes seems like a wise option to avoid a dead-on-arrival product launch, while at the same time the focus on creating an independent search index of the web that is not beholden to other companies' commercial interests is a laudable goal. Additionally, Brave Search's focus on respecting user privacy is an encouraging sign. But let's see how it all works in practice. I'll be performing most of the same tests that I used in my previous review of search engines, and some additional queries will be included to put Brave Search through its paces.


# Brave Search engine tests

**Best possible score: ⭐⭐⭐⭐**

**Worst possible score: ☆☆☆☆**

## Criterion #1: Relevancy of Results
**(Weight: 60%)**

For these tests, I'm using a private browser window in Firefox (mainly so that it doesn't show already visited links in a different color), with safe search disabled, region set to "All regions", and local search results disabled. This should create a level playing field for others that might want to test these same queries now or at some point in the future.


### Example Search #1: `mac kvm`
This query attempts to locate a fairly new but popular project on Github called "foxlet › macOS-Simple-KVM" that allows for easily running macOS on the KVM virtualization platform for Linux. Here the possibility of ambiguity exists between "Kernel-based Virtual Machine" and "Keyboard, Video and Mouse switch". There also could be problems with the language of the results, since the search terms are specific technical words or acronyms that are usually borrowed in many non-English languages without change.

#### Brave Search result | ⭐⭐⭐⭐
![Brave search result](/20210622-brave/01-mac-kvm.png)

Very good. Without having a personal profile on my search habits, Brave Search would have no way of knowing exactly what sort of "KVM" I'm looking for, but the popular Github project is appropriately high in the search results ranking. No attempts at changing or ignoring my search terms. Notice that in this case it claims that the results came from its own independent search index, which is an impressive early showing.


### Example Search #2: `atril search list`
Here's another challenging technical search that is likely to run into ambiguity problems. What I am looking for is information about the Atril PDF viewer possibly showing a summary of on-page search results in a single list or pane. This will be a challenging query, because many sites have search functionality with the word "search" on the page or as part of the URL, which is likely to throw off many search engines. Also the word _"atril"_ by itself doesn't have anything to do with PDF viewers, and it means "podium" or "lectern" in Spanish, which will help to test if the search engine is language-aware.

#### Brave Search result | ⭐☆☆☆
![Brave search result](/20210622-brave/02-atril.png)

Brave Search technically failed on this query. It receives one star for at least understanding that Atril is a PDF viewer, but its score isn't higher because most of the pages don't contain the query term `list`. Ignoring certain terms that the user specified in the search query is an especially **_bad_** trait that I sincerely hope Brave Search engineers will nip in the bud. To be fair, even Google is exhibiting this irritating behavior, but that's all the more reason for potential alternatives to not imitate it. By the way, the result I was looking for is [here](https://github.com/mate-desktop/atril/issues/238), and no amount of finessing the search terms will make Brave Search find it. So it appears that there is some work to be done here with indexing Github issues. Apart from that, the quality of the results was reasonable, and it reports that only 5% of them came from third-party indexes.


### Example Search #3: `atril instalar flatpak`

To test Brave Search's natural language awareness, let's try another query about the Atril PDF viewer, but this time in Spanish or Portuguese. I'll keep the region setting at "All regions", which is what most multilingual users would probably use. Ideally we don't want to have to specify the language for each search query throughout the day, and the search engine should generally be able to figure out the language from the search terms.

#### Brave Search result | ⭐⭐☆☆
![Brave search result](/20210622-brave/03-atril.png)

It handled the language issue fairly well, but unfortunately the pages didn't contain some of the requested keywords. Upon inspection of the page source the terms do appear, but they are not visible to a normal human user. For this query, roughly half of the results came from third party search indexes.


### Example Search #4: `atril instalar ubuntu`

Still intrigued by the issue of language flexibility, I tried another Spanish/Portuguese query.

#### Brave Search result | ⭐☆☆☆
![Brave search result](/20210622-brave/04-atril.png)

Quite poor results for this search. Most of the pages were in the wrong language, and they don't contain the term `instalar`, not even in the page source. So this is another example of **_bad_** behavior where the search engine changes terms to what it _thinks_ the user is looking for but not spelling correctly. Query terms should _never_ be silently changed or ignored without warning. Don't second-guess users; most of us know how to spell and our query terms are intentional. This behavior is particularly egregious given the fact that Brave Search does have spellchecking-- for example, I searched for `atril installlerr ubuntu` and it displayed a message: "_Showing results for **atril installer ubuntu** Search instead for **atril installlerr ubuntu**_". That is a reasonable and helpful response where an obvious spelling mistake is present, whereas the word `instalar` is _not_ a mistake. For this query, Brave only used its own search index, which reveals another facet of its immaturity.


### Example Search #5: `patagonia`

Turning to a less technical subject, here's another interesting test of a search engine's ability to return relevant results despite the ambiguity of a query. In this case, "Patagonia" could be a region in South America, or a brand of outdoor clothing.

#### Brave Search result | ⭐⭐⭐⭐
![Brave search result](/20210622-brave/05-patagonia.png)

An excellent result from Brave Search. The results cover both topic possibilities, with a helpful infobox to the side. There are also relevant news items. There does not appear to be an excessive commercial slant to the results. This is especially impressive given that Brave only used its own search index for this query.


### Example Search #6: `"best place to stay in the loop about related updates"`
Sometimes I remember a specific and unique phrase from an article or comment that I read somewhere, and to find it again I search for that phrase surrounded by quotation marks. This particular example is from a comment in a relatively obscure blog  post that appeared on the same day as the writing of this article. This will be a challenging test of the speed at which a search engine's crawler (or a third-party source source) locates and indexes new content, as well as the extents of its activity in lesser known parts of the internet. This will also show whether the search engine respects the quotation marks to find specific phrases.

#### Brave Search result | ⭐☆☆☆
![Brave search result](/20210622-brave/06-recent-quote.png)

Unfortunately Brave didn't find it. But Google did, so Brave would have done well to use that for this case where no native results were found. But it still earns a star for at least respecting the quotation marks and not returning a bunch of random pages that contain any one of those single keywords by itself.


### Example Search #7: `"Not only did SiFive announce"`
I decided to give Brave Search another chance to see if it could find a specific phrase in an additional relatively obscure recent blog post from the same day as this writing.

#### Brave Search result | ⭐⭐⭐⭐
![Brave search result](/20210622-brave/07-recent-quote.png)

Excellent, it found it. I don't quite understand why it says that "50% of results mixed from third-parties", since there was only one result. Or is it including the news items in the tally of total results?


### Example Search #8: `brave search`
Let's give Brave Search one more test at finding very recent pages, which tends to be challenging for the underdog search engines. Of course, I'm assuming that the meta nature of this query isn't receiving special treatment... For this search, we'll be trying the date range filter, which I am extremely happy to see even in this early beta stage of Brave's search product.

#### Brave Search result | ⭐⭐⭐⭐
![Brave search result](/20210622-brave/08-recent-search.png)

Very good results. The authoritative source appears first, followed by reputable technology websites and news items. And the results are indeed from the date range that I specified.


### Example Search #9: `zwopper blue` (Image Search)
In my testing of many search engines, it appears that image search in a unique challenge. Let's see how Brave Search fares when searching for my favorite wallpaper. All I remember is that it has something about "Zwopper" in the filename, and its dominant color is blue.

#### Brave Search result | ⭐⭐⭐⭐
![Brave search result](/20210622-brave/09-image-search.png)

Nice! Found it. Notice that it also has a handy button to directly load the file, which is something that DuckDuckGo also offers, whereas Google has removed that functionality. Brave's image search also allows for filtering by image size, type, color, and layout, which is very impressive at this early stage. It would be nice if it also allowed filtering by image license type.


## Criterion #2: Instant Answers
**(Weight: 30%)**

Increasingly, search engines are used to obtain quick bits of information without needing to even visit a separate website. Providing instant answers depends on large databases of information that are maintained either by the search engine company or by a separate organization, which needs to be coupled with intelligent natural language processing. These next tests will attempt to identify Brave Search's ability to offer relevant quick facts, and if so, what sources they tap into.

### Instant Answer Request #1: `sycophant`
Instant word definitions are a staple of instant answers. Of course, maintaining up-to-date and comprehensive dictionary entries requires a considerable amount of resources.

#### Brave Search result | ⭐⭐☆☆
![Brave search result](/20210622-brave/01-sycophant.png)

Acceptable results from the infobox, although it loses points for being too rambling and verbose and for not including a pronunciation button or etymological tools. Brave says it only used its independent index for this page of results, and the source of the infobox appears to be Wikidata.


### Instant Answer Request #2: `dreadful`
Let's try another word definition.

#### Brave Search result | ⭐⭐⭐☆
![Brave search result](/20210622-brave/02-dreadful.png)

This time it gave me legitimate terse dictionary definitions from merriam-webster.com, which is more like what I would expect from a good instant answer. However, it looses a point because I had to manipulate the query a bit-- a search for just `dreadful` didn't return any quick answer, but I found that adding `def` or `definition` or `define` makes it reliably return a definition for any word. A small nitpick would be regarding the location of the infobox, it feels inconsistent to place it above the normal search results instead of at the top right like other infoboxes we've seen in this test.


### Instant Answer Request #3: `sycophant in spanish`
For translators and multilingual writers, quick translations are a huge boon. In order to offer a concise and accurate translation, a search engine must have access to a formidable database of translation equivalents, and even artificial intelligence and machine translators can come into play.

#### Brave Search result | ☆☆☆☆
![Brave search result](/20210622-brave/03-translation.png)

Unfortunately no joy with this query. I tried other terms too like `translate` and `translation` to see if I could make it react, but it looks like Brave Search does not yet tap into any translation databases or APIs. I really hope this changes in the future.


### Instant Answer Request #4: `capital oregon`
This should be a relatively easy candidate for an instant answer to provide a quick fact based on readily available public information.

#### Brave Search result | ⭐⭐⭐⭐
![Brave search result](/20210622-brave/04-capital.png)

Well, it gave me the correct instant answer, although it second-guessed my search term and also gave me information on the "capitol" building, in contrast with the "capital" of the state.


### Instant Answer Request #5: `$5/lb in gbp/kg`
This one is extremely difficult. First, the search engine has to parse a plethora of symbols and acronyms, then it has to access up-to-date currency exchange values, and then it has to convert between different units of measurement.

#### Brave Search result | ☆☆☆☆
![Brave search result](/20210622-brave/05-complex-conversion.png)

Unfortunately Brave Search failed to give a relevant answer, although it's good to see that it does offer basic conversion.


### Instant Answer Request #6: `2 lb in kg`
Maybe the complex conversion was too much to ask of a beta product. So let's try an easier conversion instead.

#### Brave Search result | ⭐⭐⭐⭐
![Brave search result](/20210622-brave/06-simple-conversion.png)

No complaints here. But...


### Instant Answer Request #7: `3 lb in kg`
Something is still janky with Brave Search's natural language processing for instant answers.

#### Brave Search result | ☆☆☆☆
![Brave search result](/20210622-brave/07-simple-conversion.png)

The screenshot says it all.


## Criterion #3: Other characteristics
**(Weight: 10%)**

#### Brave Search | ⭐⭐⭐⭐

For this slightly more subjective category, I have to give Brave 4 out of 4 stars. First of all, the importance of Brave's endeavor to actively crawl the web and create its own relevant and independent search index can not be overemphasized. This alone earns it a high rating, and makes it a serious competitor to DuckDuckGo, which is the only semi-competent alternative to Google currently on the market. I sincerely applaud the developers and engineers for taking on this gargantuan task, and I commend them for the considerable progress made thus far, as evidenced by the percentages of results that they show as coming from their own index. Speaking of those statistics, there are more of them under the "hamburger" menu, which feels like a rather odd location, since according to most conventions that control is reserved for interactive elements, not pure informational display. I also admire Brave's pragmatism at offering a mix of third-party search results to supplement the current deficiencies of its own search index. And importantly, it apparently uses Google results where needed, as compared to DuckDuckGo and its reliance on Bing's vastly inferior index and logic.

Another extremely positive aspect of Brave Search is its search filtering functionality, with correctly working time range filters, and comprehensive image search filters. It's great to see that they've prioritized this functionality right out of the gate, which is something that other alternative search engines have taken years to implement, and in the case of DuckDuckGo still doesn't work correctly.

One odd limitation of Brave Search is that it currently does not have any additional pages or infinite scrolling of search results. Results are currently limited to 20, and no more. The engineer that answered my private email about the issue said that this may change in the future. But for now, Brave Search is apparently focused on the typical search engine user that doesn't bother to look past the first few results and gives up or changes the query if relevant results don't appear towards the top. While I understand the usage patterns of most users, this feels like a missed opportunity for Brave Search, given that the bulk of its users at least in the near future will overwhelmingly be power users that often perform deep-dive searches, analyzing page after page of results on a single search query.


# Conclusion

## Brave Search Overall Score | ⭐⭐⭐☆

As I mentioned at the beginning, I am very excited by the current implementation and stated goals of Brave Search. I started using it prepared to be disappointed by another Bing clone that regurgitates private but ultimately poor search results from its parent search index. But I was pleasantly surprised by the features of the Brave Search engine and the relative maturity of its own search index, and I feel that the inclusion of third-party search indexes is a very necessary and practical complement that will make Brave Search a viable option for many more potential users right now. Its current beta product scored an overall weighted average of 2.6 out of 4 stars in my tests, which is a very respectable grade on its first day of public operation. Brave Search has definitely hit the ground running. Keep your eyes on this one.



## ✍ Share your thoughts on this article!
**➠[https://gitlab.com/libretechtips/libretechtips.gitlab.io/issues/3](https://gitlab.com/libretechtips/libretechtips.gitlab.io/issues/3)**
