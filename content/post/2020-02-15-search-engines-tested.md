---
title: "Detailed tests of search engines: Google, Startpage, Bing, DuckDuckGo, metaGer, Ecosia, Swisscows, Searx, Qwant, Yandex, and Mojeek"
date: 2020-02-15
tags: ["reviews", "search", "Google", "Startpage", "Bing", "DuckDuckGo", "metaGer", "Ecosia", "Swisscows", "Searx", "Qwant", "Yandex", "Mojeek"]
---

Since my last in-depth [comparison review of alternative search engines in 2014](https://libretechtips.gitlab.io/review-duckduckgo-may-2014-redesign-compared-to-google-bing-yandex/), a lot has changed, and a lot has stayed the same. Google is appearing as a loan-verb in more and more languages due to its continued dominance in the search engine market. But at the same time, Google is being increasingly demonized by privacy focused users. An even more interesting development is the trend of complaints that Google's algorithm is producing results that are  less relevant and more indicative of artificial stupidity than artificial intelligence. I belong in this latter camp, as I am more of a pragmatist than a privacy pundit. I simply want the best search results with minimal effort and no nonsense. Back in my 2014 article, I was hopeful that DuckDuckGo was quickly becoming a viable and attractive alternative to Google. While DuckDuckGo continues to be the darling of privacy conscious users and is enjoying more popularity than ever, I am concerned that its core search infrastructure and algorithms have largely stagnated. Since my last article, many other alternatives have cropped up, bringing some very interesting features and concepts, but it still remains to be seen if they offer acceptable results in the fundamentally important area of _relevant search results_. This comparison sets out to analyze and compare the current batch of alternatives in 2020.



# The options

## [Google.com](https://google.com)
Google needs no introduction. It's massive, it's omniscient, and it's increasingly frustrating for discriminating users that simply want high-quality, neutral search results. Its principal advantage is its *massive* search index and lightning speed at which the crawler picks up fresh content. A major concern even for pragmatists is the "search bubble" phenomenon, where Google tends to present results tailored to each user's individual beliefs and preferences, correlated to the massive data trove that Google has on each individual.

## [Startpage.com](https://startpage.com)
Startpage's claim to fame is that it offers access to Google's core search algorithms and massive search index with anonymity. As they explain it: _"You can’t beat Google when it comes to online search. So we’re paying them to use their brilliant search results in order to remove all trackers and logs. The result: The world’s best and most private search engine."_ Startpage has developed a small but fiercely loyal base of users that swear by the quality of its search results while championing the anonymity that it offers. On the other hand, it also feels like a bit of a one-trick pony, since its entire model depends on the continuity of the arrangement with Google to reuse its search results. Apart from that, Startpage offers a fairly bare-bones, no-nonsense search experience.

## [Bing.com](https://bing.com)
After Google, Bing runs one of the most active independent web crawlers for maintaining its own index of web content.[^1] This review will test the extent of that index, as well as the intelligence of Bing's search algorithms and additional features. Microsoft at certain points in its history has aggressively promoted Bing, and Bing even partners with many of the other search engines mentioned in this article to serve as the primary source of anonymous results for the much smaller alternatives that lack the necessary resources to create and/or rely on their own search indexes.  Even the once mighty Yahoo now uses Bing's infrastructure, which is why it is not addressed separately in this review.
[^1]: https://deviceatlas.com/blog/most-active-bots-and-crawlers-web

## [DuckDuckGo.com](https://duckduckgo.com)
DuckDuckGo has the laudable tagline of **"Privacy, simplified"**, with categorical and unequivocal claims of _"We don’t store your  
personal information. Ever."_ and "_We don’t track you — period."_ While such principals are music to the ears of privacy pundits, DuckDuckGo's principal function is still to find stuff on the web, so its privacy values would become a moot point if its search results were inadequate. Disappointingly, DuckDuckGo still continues to source its main search results from Bing and Yahoo,[^2] while using its DuckDuckBot crawler mainly to eliminate spam and malicious content from the search results.[^3] So while DuckDuckGo paints a bright picture for search privacy, it does little to offer alternatives in terms of crawling unique content or ranking results with a superior algorithm, which seems like a bit of an antithesis to DuckDuckGo's core mission of being a search engine. But on the positive side, it does offer an attractive interface with some helpful additions for disambiguation and instant answers.
[^2]: https://help.duckduckgo.com/duckduckgo-help-pages/results/sources/
[^3]: https://help.duckduckgo.com/duckduckgo-help-pages/results/duckduckbot/

## [metaGer.org](https://metager.org)
Metager is an obscure option that is billed as a meta-search engine that anonymizes the queries made to commercial search indexes while at the same time combining its own small-scale web crawlers.[^4] It presents the results with a label indicating the source. Although claiming to use a wide variety of sources, the results that I received in my testing appeared to come almost exclusively from Bing. Metager offers the unique option of opening results anonymously via its proxy service. An added benefit of Metager is that its engine is open source and available on [GitLab](https://gitlab.metager.de/open-source/MetaGer).
[^4]: https://metager.org/hilfe

## [Ecosia.org](https://ecosia.org)
Ecosia is a much more obscure search engine, with its main claim to fame being that it uses profits to plant trees. Ecological issues aside, it uses _"Bing's search technology, enhanced with Ecosia's own algorithms."_ [^5] It will be interesting to observe in the forthcoming tests in this article if Ecosia manages to offer any improvements in search results relevancy over its Bing source. For privacy-focused users, Ecosia could offer some unique [advantages](https://info.ecosia.org/privacy), such as anonymizing search queries and not tracking its users.
[^5]: https://ecosia.zendesk.com/hc/en-us/articles/206153381-Where-do-Ecosia-s-search-results-come-from-

## [Swisscows.ch](https://swisscows.ch)
Swisscows is an intriguing alternative search engine based out of an [ultra-secure facility](https://swisscows.ch/en/datacenter) in Switzerland, which should make it appealing to privacy-conscious users. Indeed, Swisscows obsesses over the [privacy](https://company.swisscows.ch/en/vision/no-monitoring) and [anonymity](https://swisscows.ch/en/about#privacy-data) of its search technology, while at the same time touting many [technical innovations](https://company.swisscows.ch/en/products) in its search algorithms. The most visible difference is the semantic tag cloud that appears to the side of search results, which is supposed to make drilling down for specific information more easy. Another unique characteristic of Swisscows is that it claims to filter out adult content from its results. It is unclear how Swisscows relies on Bing as a source compared to its own web crawler. Older information suggests that it mainly relied on Bing,[^6] whereas currently Swisscows places great emphasis on its own crawler: _"The Swisscows Crawler is an important and central module for analyzing any kind of information content. [...]
All information available on the web, such as documents and simple web pages, are efficiently indexed. [...] The crawler provides best completeness and timeliness. The advanced crawler visits the web content at least once within 48 hours."_  [^7]
[^6]: https://serpwatch.io/blog/alternative-search-engines/ and https://business.financialpost.com/technology/european-privacy-search-engines-aim-to-challenge-google
[^7]: https://company.swisscows.ch/en/products

## Searx ([information](https://asciimoo.github.io/searx/) | [public instances](https://searx.space/))
Searx is a relatively new option (since around 2015) that presents a unique and extremely promising alternative in the search engine space. To begin with the negative points, it does not have its own crawler, and indeed it doesn't even offer a dedicated public search service. But its disadvantages can also be seen as distinct advantages. Searx is not dependent on the whims of any particular parent search engine's index, since it is designed as a meta search engine to aggregate and anonymize the queries and results from a plethora of commercial search engines, including Google itself. It can be self-hosted on a VPS instance or even via Docker on a local workstation. There is also a [long list](https://searx.space/) of hosted Searx instances that are open to the public, although their speed and uptime is highly variable. Searx will probably never achieve mass acceptance or even come close to the userbase of alternatives like DuckDuckGo, but for those in the know, it is an intriguing alternative.

## [Qwant.com](https://qwant.com)
Qwant runs one of the larger and more active crawlers and search indexes among the alternative search engine providers. Yet, as proof of the gargantuan nature of the tasks required to create a useful and current search index, Qwant, like many others on this list, still supplements its results with ones from Bing's index.[^8] For the privacy-conscious, Qwant also promises not to track its users or their search habits. It also offers different categories of results and ways of filtering them, together with optional news thumbnails on the frontpage.
[^8]: https://help.qwant.com/help/overview/how-does-qwant-index-the-web/

## [Yandex.com](https://yandex.com)
Yandex is notable because it maintains its own large, independent search index, and is ranked as the fifth largest search engine worldwide.[^9] As a matter of fact, Yandex offers an entire suite of cloud services, such as email, machine translation, and cloud storage, similar to Google and Microsoft. Yandex is primarily dominant in Russia, but it also is usable for English and other international language searches. Anecdotally, on my own webservers I see a lot of activity from Yandex crawler bots.
[^9]: https://www.imperva.com/blog/know-your-top-10-bots/

## [Mojeek.com](https://mojeek.com)
Mojeek is a significant alternative search engine thanks to its goal of being  independent. To achieve this, Mojeek runs its own crawler to maintain an independent search index that reportedly has indexed over 2 billion pages.[^10] They make some good points about the importance of true independence from other search engines: _"Although we very much respect these alternative engines (usually due to our overlapping ethos), they still rely upon a mainstream engine to provide them with their organic search results, which in turn come complete with the bias already present within those results or engine. When you use Mojeek you don't have to worry about first or second hand bias, our index has been created from the ground up, and our results are ranked according to our own algorithms and no-one elses._ [sic]_"_ [^11] The tests in this article should help to reveal if Mojeek's efforts in this space are enough to make it a practical alternative search engine. As for Mojeek's [privacy stance](https://blog.mojeek.com/2018/10/search-that-does-not-follow-you-around.html), it appears to be very good.
[^10]: https://www.mojeek.com/about/technology.html
[^11]: https://blog.mojeek.com/2018/08/independent-and-unbiased-search-results.html



# Search Engine Tests

Now for the fun part. Let's put these search engines through their paces, with a strong focus on the relevancy of their search results, any quirks that are exhibited, and the intelligence and presentation of their instant answers. All tests will be conducted in a Firefox "Private Window", although in the case of Google, a [study by DuckDuckGo](https://spreadprivacy.com/google-filter-bubble-study/) claims that private or incognito browsing makes little difference in providing generic results and thus freedom from the ["filter bubble"](https://en.wikipedia.org/wiki/Filter_bubble) effect. The browser language is set to US English, and the search engines will be set to prefer English language results with no regional bias where possible, although many only set the language based on the country. Also where possible, "Safe Search" options are set to a moderate level.

In the case of the highly configurable Searx engine, I have enabled the "General" and "Science" categories by default, with the only Science entry being Wolfram|Alpha, and the following engines enabled under General: `wikipedia`, `bing`, `ddg definitions`, `wikidata`, `google`, `startpage`, `yandex`, `dictzone`, `mymemory translated`. I am using a reasonably well performing [publicly hosted instance](https://stats.searx.xyz/) that is running the recently released [0.16 version](https://github.com/asciimoo/searx/releases/tag/v0.16.0) of Searx. 


## Criterion #1: Relevancy of Results
**(Weight: 60%)**


### Example Search #1: `mac kvm`
This query attempts to locate a fairly new but popular project on Github called "foxlet › macOS-Simple-KVM" that allows for easily running macOS on the KVM virtualization platform for Linux. Here the possibility of ambiguity exists between "Kernel-based Virtual Machine" and "Keyboard, Video and Mouse switch". There also could be problems with the language of the results, since the search terms are specific technical words or acronyms that are usually borrowed in many non-English languages without change.

#### Google | ⭐⭐⭐⭐
![Google image](/20200215-search/01/google.png)

Very good. The second result is the desired one. No attempts at changing or ignoring my search terms.

#### Startpage | ⭐⭐⭐⭐
![Startpage image](/20200215-search/01/startpage.png)

Very good. Largely the same as Google's results, but notice the different order.

#### Bing | ⭐⭐☆☆
![Bing image](/20200215-search/01/bing.png)

Disappointing results, especially the one for `linux-kvm.org`, which doesn't even contain the term `mac`.

#### DuckDuckGo | ⭐⭐⭐☆
![DDG image](/20200215-search/01/ddg.png)

OK results, I would have preferred less emphasis on switches and more on virtual machines, but at least the desired result was in the fifth spot.

#### metaGer | ⭐☆☆☆
![metaGer image](/20200215-search/01/metager.png)

Poor results, with a heavy focus on commercial sales, especially Amazon.

#### Ecosia | ⭐⭐☆☆
![Ecosia image](/20200215-search/01/ecosia.png)

Not very good results, again with a focus on sales instead of information, although the desired Github project appeared farther down.

#### Swisscows | ⭐⭐⭐☆
![Swisscows image](/20200215-search/01/swisscows.png)

Reasonable results, similar to those of DuckDuckGo.

#### Searx | ⭐⭐⭐⭐
![Searx image](/20200215-search/01/searx.png)

Very good results, possibly better than Google's, by virtue of showing the Github projects and open source solutions first, and toward the end including a result for KVM switches too.

#### Qwant | ⭐⭐☆☆
![Qwant image](/20200215-search/01/qwant.png)

Mediocre results similar to the Bing-sourced search engines, focuses on Amazon products, although it did find the desired Github project toward the end.

#### Yandex | ⭐☆☆☆
![Yandex image](/20200215-search/01/yandex.png)

Didn't find the Github project I was looking for, but it did provide reasonably balanced results, although one was in Russian despite me specifying English.

#### Mojeek | ⭐☆☆☆
![Mojeek image](/20200215-search/01/mojeek.png)

Very poor results, one page was in Chinese, and another one was a garbage domain in Spanish.


### Example Search #2: `atril search list`
Here's another challenging technical search that is likely to run into ambiguity problems. What I am looking for is information about the Atril PDF viewer possibly showing a summary of on-page search results in a single list or pane. This will be a challenging query, because many sites have search functionality with the word "search" on the page or as part of the URL, which is likely to throw off many search engines. Also the word _"atril"_ doesn't have anything to do with PDF viewers, and it means "podium" or "lectern" in Spanish, which might confuse the algorithms that look for English results as I specified.

#### Google | ⭐⭐⭐☆
![Google image](/20200215-search/02/google.png)

Google did find the Github issue that I was looking for and put it in the first slot, which is impressive. However, it loses points for ignoring the search term `list` in some of the other results, and it appears to have used the synonym `find` instead of `search` for some results, which I did not ask for. Stop second-guessing me, Google!

#### Startpage | ⭐☆☆☆
![Startpage image](/20200215-search/02/startpage.png)

In this case, Startpage's results are drastically different from Google's, no doubt because of the elimination of the filter bubble. Unfortunately, its results were also irrelevant, with it barely figuring out toward the end that I was searching for the Atril PDF viewer, and ignoring some search terms.

#### Bing | ☆☆☆☆
![Bing image](/20200215-search/02/bing.png)

Bing really fell on its face with this query. The results speak for themselves.

#### DuckDuckGo | ☆☆☆☆
![DuckDuckGo image](/20200215-search/02/ddg.png)
DuckDuckGo's results were similarly awful. Notice how it simply ignores or guesses at the meaning of the search term `atril` without even asking for my consent.

#### metaGer | ☆☆☆☆
![metaGer image](/20200215-search/02/metager.png)

We're seeing a trend here for this challenging search query. Total fail.

#### Ecosia | ☆☆☆☆
![Ecosia image](/20200215-search/02/ecosia.png)

Identical to DuckDuckGo's results in the first five slots, and fails the way the other engines do that source their results from Bing.

#### Swisscows | ☆☆☆☆
![Swisscows image](/20200215-search/02/swisscows.png)

Another complete flop, although its first result is different from the other engines that depend on Bing, which might be evidence of the influence of Swisscows' own crawler/index.

#### Searx | ⭐⭐☆☆
![Searx image](/20200215-search/02/searx.png)

Well, at least it figured out that I was searching for the Atril PDF viewer in several results, and it did find the specific Github issue I was looking for. Not too bad, considering the performance of the competition.

#### Qwant | ☆☆☆☆
![Qwant image](/20200215-search/02/qwant.png)

Qwant's results are somewhat different but still seem similar to Bing's for this query, and they are mostly irrelevant. Another fail.

#### Yandex | ☆☆☆☆
![Yandex image](/20200215-search/02/yandex.png)

Yandex also failed spectacularly, with a specific focus on the Spanish meaning of "_atril_" and links that contain `listado`.

#### Mojeek | ⭐☆☆☆
![Mojeek image](/20200215-search/02/mojeek.png)

Mojeek also failed to find the desired page, and it ignored or misinterpreted several of the terms, but at least it returned a few results about the Atril PDF viewer.


### Example Search #3: `patagonia`

Turning to a less technical subject, here's another interesting test of a search engine's ability to return relevant results despite the ambiguity of a query. In this case, "Patagonia" could be a region in South America, or a brand of outdoor clothing.

#### Google | ⭐☆☆☆
![Google image](/20200215-search/03/google.png)

Google's results here demonstrate the highly commercial focus. Apart from the fact that so much space is wasted with the expanded sitemap and the useless Twitter feeds, all of the results on the first page were about Patagonia clothing, with the exception of the images and videos farther down.

#### Startpage | ⭐☆☆☆
![Startpage image](/20200215-search/03/startpage.png)

Startpage also makes atrocious use of the available space, and again shows the obvious commercial focus of its Google-sourced results. The first seven results were all about the clothing company. The map, on the other hand, was about a one-horse town in Arizona that I suspect only its residents have ever heard of.

#### Bing | ⭐⭐☆☆
![Bing image](/20200215-search/03/bing.png)

At first glance, Bing's results page looks promising because it offers disambiguation links at the top. Except for all of them are about different types of Patagonia clothing. The second result at least does show a Wikipedia article about the region in South America, but no more results are visible without scrolling due to the poor utilization of space and the image thumbnails.

#### DuckDuckGo | ⭐⭐⭐☆
![DDG image](/20200215-search/03/ddg.png)

Pretty good. It shows a decent mix of clothing and geographical results, with a Wikipedia infobox about the region off to the side. DuckDuckGo would have received a perfect score, except for the fact that it has regressed in its disambiguation functionality. In my 2014 test of this same query, DDG [showed the meaning category automatically](https://libretechtips.gitlab.io/20140527-search/10_ddg.png)
, thus helping the user to see that the query is too vague and offering better options. But this time, the "Meanings" section is hidden by default.

#### metaGer | ⭐⭐☆☆
![metaGer image](/20200215-search/03/metager.png)

Pretty balanced overall, except for one garbage result for an available name containing the term `patagonia`. Unfortunately no infoboxes, but a Wikipedia disambiguation list appears in expanded state to the right. It would have been better to expand it by default.

#### Ecosia | ⭐⭐⭐⭐
![Ecosia image](/20200215-search/03/ecosia.png)

Ecosia actually did very well with this search. It figured out both meanings of `patagonia` and showed infoboxes for both of them, as well as appropriately sized pictures.

#### Swisscows | ⭐⭐⭐⭐
![Swisscows image](/20200215-search/03/swisscows.png)

Swisscows also aced this test thanks to its smart semantic filters and sensible utilization of space.

#### Searx | ⭐⭐⭐⭐
![Searx image](/20200215-search/03/searx.png)

Searx performed very well thanks to its balanced presentation of the two meanings and the information-dense display. The infoboxes were also very helpful, with information about the company and the region, and farther down there was even a "Suggestions" box with more related terms to fine-tune the search.

#### Qwant | ⭐⭐⭐☆
![Qwant image](/20200215-search/03/qwant.png)

Sensible results, although the infobox feels too big and takes up a lot of space.

#### Yandex | ⭐⭐⭐⭐
![Yandex image](/20200215-search/03/yandex.png)

A very good showing for Yandex, with an information-dense page layout, good balance, relevant pictures, and a great infobox with "Sightseeings".

#### Mojeek | ⭐⭐⭐⭐
![Mojeek image](/20200215-search/03/mojeek.png)

An excellent mix of balanced results, some unique to Mojeek compared with the other engines, an infobox, and relevant news stories.


### Example Search #4: `"I personally find Java to be verbose"`
Sometimes I remember a specific and unique phrase from an article or comment that I read somewhere, and to find it again I search for that phrase surrounded by quotation marks. This particular example is from a comment in a relatively obscure blog  post that appeared two days ago from the time of this writing. This will be a challenging test of the speed at which a search engine's crawler (or source provider) locates and indexes new content, as well as the extents of its activity in lesser known parts of the internet. This will also show whether the search engine respects the quotation marks to find specific phrases.

#### Google | ⭐⭐⭐☆
![Google image](/20200215-search/04/google.png)

Found it. Exactly what I asked for, nothing more, nothing less. But it loses a point for obscuring the complete URL.

#### Startpage | ⭐☆☆☆
![Startpage image](/20200215-search/04/startpage.png)

Weirdly, Startpage did not locate the requested post, despite the fact that its Google sources already indexed it. But at least it admits the failure and doesn't show any other extraneous results, for which it earns a star. I have observed this behavior with several other queries too, where Google shows an obscure page that Startpage doesn't. My theory is that Google does not pass 100% live results to Startpage, but rather a cached snapshot of its index state. I have tried playing with Startpage's family filters, and it doesn't always make a difference when they are turned off.

#### Bing | ☆☆☆☆
![Bing image](/20200215-search/04/bing.png)

Complete failure. In addition to not knowing about the requested post, Bing offers 15,600 results that contain some of the search terms, but completely ignores the quotation marks. If such a thing as negative stars existed, Bing would receive four of them here.

#### DuckDuckGo | ☆☆☆☆
![DDG image](/20200215-search/04/ddg.png)

DuckDuckGo fails in a similar way to its Bing sources, but with less false positives.

#### metaGer | ☆☆☆☆
![metaGer image](/20200215-search/04/metager.png)

Interestingly, metaGer fails in the exact same way as DuckDuckGo.

#### Ecosia | ☆☆☆☆
![Ecosia image](/20200215-search/04/ecosia.png)

Ecosia also shows the same three irrelevant results as DuckDuckGo and Metager.

#### Swisscows | ☆☆☆☆
![Swisscows image](/20200215-search/04/swisscows.png)

Swisscows also continues the same trend as the other Bing-sourced engines, but adds one more irrelevant result of its own.

#### Searx | ⭐⭐⭐⭐
![Searx image](/20200215-search/04/searx.png)

Unlike Startpage, Searx nailed this one because it essentially acts as a search proxy to pull live results from Google. From Google's point of view, the Searx engine looks the same as a live user of Google.com, so it receives the most up-to-date results that Google has to offer. Additionally, Searx shows the full URL of the result, earning it top results in this category.

#### Qwant | ☆☆☆☆
![Qwant image](/20200215-search/04/qwant.png)

Qwant is obviously using Bing's index for these irrelevant results.

#### Yandex | ☆☆☆☆
![Yandex image](/20200215-search/04/yandex.png)

Yandex weirdly presents only some irrelevant image results.

#### Mojeek | ☆☆☆☆
![Mojeek image](/20200215-search/04/mojeek.png)

Mojeek presents a large selection of uniquely irrelevant results, so apparently it doesn't respect queries enclosed in quotation marks either.



## Criterion #2: Instant Answers
**(Weight: 30%)**

Increasingly, search engines are used to obtain quick bits of information without needing to even visit a separate website. Just as the alternative search engines usually depend on another large search company's search index, providing instant answers also depends on large databases of information that are maintained either by a large search engine or by a separate organization. These next tests will attempt to identify the search engines' ability to offer relevant quick facts, and if so, what sources they tap into.

Just as with the search results tests, for optimal instant answers the search engines are set to prefer the English language with no regional bias where possible, although many only set the language based on the country. This is especially important for Bing, which offers very different instant answer experiences depending on the geographical region that is chosen. For this test, Bing gave much better instant answers when set to the "United States - English" region.


### Instant Answer Request #1: `sycophant`
Instant word definitions are a staple of instant answers. Of course, maintaining up-to-date and comprehensive dictionary entries requires a considerable amount of resources, which puts the smaller players in this test at a disadvantage.

#### Google | ⭐⭐⭐⭐
![Google image](/20200215-search/05/google.png)

Extremely comprehensive, with a concise, accurate Oxford dictionary definition and a pronunciation button. Synonyms are also shown, with an expander for word origin, other definitions, and historical word usage statistics.

#### Startpage | ⭐⭐☆☆
![Startpage image](/20200215-search/05/startpage.png)

Not very good. It should be noted that, although Startpage does have official access to Google's search index and algorithms, it does _not_ have access to Google's instant answers. In this case, it offered an infobox with the beginning of the Wikipedia entry for "Sycophant", which contains a long, protracted discussion of the etymological roots of the word, but only briefly touches on the modern definition.

#### Bing | ⭐⭐⭐⭐
![Bing image](/20200215-search/05/bing.png)

Excellent, Bing appears to use the same Oxford dictionary database as Google, and it also offers an expander for translations and word origin.

#### DuckDuckGo | ⭐⭐⭐☆
![DDG image](/20200215-search/05/ddg.png)

Very good. DuckDuckGo shows an instant answer with Wordnik definitions, which I usually find to be concise and accurate. It also offers a pronunciation icon, but for this particular word it didn't work.

#### metaGer | ☆☆☆☆
![metaGer image](/20200215-search/05/metager.png)

Unfortunately metaGer failed at offering instant answers. Of course, some information can be gleaned from the summaries of the search results, but a front-and-center instant answer would be much better.

#### Ecosia | ⭐☆☆☆
![Ecosia image](/20200215-search/05/ecosia.png)

Although Ecosia shows a Wikipedia infobox similar to what appeared on Startpage, the visible text was even less relevant to the definition of the word, instead centering exclusively on the etymology. 

#### Swisscows | ☆☆☆☆
![Swisscows image](/20200215-search/05/swisscows.png)

Swisscows made no attempt at offering instant answers. I also attempted to manipulate it with other keywords such as `define sycophant` and `sycophant definition`, but it didn't help.

#### Searx | ⭐⭐⭐☆
![Searx image](/20200215-search/05/searx.png)

Although Searx still showed the same rambling Wikipedia entry in an infobox, below that there was another one with a very good summary of facts from Wolfram|Alpha, including a concise definition, word origins, and word usage history. I just wish that this box was presented above the Wikipedia one.

#### Qwant | ☆☆☆☆
![Qwant image](/20200215-search/05/qwant.png)

Qwant was unhelpful, with no instant definitions, even when adding other trigger words such as `definition` or `define`.

#### Yandex | ☆☆☆☆
![Yandex image](/20200215-search/05/yandex.png)

Yandex was also unhelpful, and even its normal search results were weird, with a bunch of images showing the word "sycophant".

#### Mojeek | ⭐☆☆☆
![Mojeek image](/20200215-search/05/mojeek.png)

Mojeek tried to give an instant answer, but like others on this list, it was limited to the poorly written Wikipedia entry, and it specifically showed the least relevant portion of the summary.


### Instant Answer Request #2: `sycophant in spanish`
For translators and multilingual writers, quick translations are a huge boon. In order to offer a concise and accurate translation, a search engine must have access to a formidable database of translation equivalents, and even artificial intelligence and machine translators can come into play.

#### Google | ⭐⭐⭐☆
![Google image](/20200215-search/06/google.png)

Google is usually a reliable and quick source of translations, and it even runs one of the most relatively effective machine translators that uses artificial intelligence and neural networks to give an approximate translation of entire paragraphs or webpages. But it loses a point for this particular example, because the term _"sicofante"_ that is presented is more of a theoretical translation, but it isn't a natural or common term in Spanish.

#### Startpage | ☆☆☆☆
![Startpage image](/20200215-search/06/startpage.png)

Like with definitions, Startpage unfortunately misses out on the wizardry of Google's translation system.

#### Bing | ⭐⭐⭐⭐
![Bing image](/20200215-search/06/bing.png)

Bing did a great job here, with an accurate translation and audible pronunciation help.

#### DuckDuckGo | ☆☆☆☆
![DDG image](/20200215-search/06/ddg.png)

DuckDuckGo also failed to offer an instant answer for this query.

#### metaGer | ☆☆☆☆
![metaGer image](/20200215-search/06/metager.png)

metaGer also failed to offer an instant answer for this query.

#### Ecosia | ☆☆☆☆
![Ecosia image](/20200215-search/06/ecosia.png)

Ecosia made a brain-dead, unhelpful attempt at an instant answer: "_**sycophant [es]**_"

#### Swisscows | ☆☆☆☆
![Swisscows image](/20200215-search/06/swisscows.png)

Swisscows has a **Translator** tab that runs the Yandex machine translator, but unfortunately it doesn't automatically trigger it, even when including words like `translation` in the query.

#### Searx | ⭐⭐⭐☆
![Searx image](/20200215-search/06/searx.png)

Good. Searx detects that I am looking for a translation even without explicitly typing the word `translation`, and it offers a very accurate translation courtesy of Wolfram|Alpha. It looses a point for not offering audible pronunciation help.

#### Qwant | ☆☆☆☆
![Qwant image](/20200215-search/06/qwant.png)

Qwant makes no attempt at providing a translation.

#### Yandex | ☆☆☆☆
![Yandex image](/20200215-search/06/yandex.png)

Like Bing, Yandex has its own advanced AI machine translator running on its own infrastructure, but unfortunately its search algorithms are too primitive to understand when the user is requesting a translation.

#### Mojeek | ☆☆☆☆
![Mojeek image](/20200215-search/06/mojeek.png)

Mojeek makes no attempt at providing a translation.


### Instant Answer Request #3: `capital oregon`
This should be a relatively easy candidate for an instant answer that provides a quick fact based on readily-available public information.

#### Google | ⭐⭐⭐⭐
![Google image](/20200215-search/07/google.png)

Perfect, the capital city of Salem appears prominently, with pictures, maps, population information, and other concise facts.

#### Startpage | ⭐⭐☆☆
![Startpage image](/20200215-search/07/startpage.png)

Not very good. Although it does detect that I'm looking for a quick fact, it simply present a summary of the Wikipedia entry for Salem, and the actual name of the city is hidden under the expander.

#### Bing | ⭐⭐⭐⭐
![Bing image](/20200215-search/07/bing.png)

Very good, the capital city of Salem appears prominently, with pictures, maps, and other information.

#### DuckDuckGo | ⭐⭐☆☆
![DDG image](/20200215-search/07/ddg.png)

Similar to Startpage, DuckDuckGo presents an infobox with the Wikipedia entry for Salem, but it's too verbose and rambling to be of much use, and it focuses more on the capitol building than on the actual name of the city. Again, it appears that DDG is tripping over itself by changing search terms (`capital` vs. `capitol`) without the user's consent.

#### metaGer | ⭐⭐⭐☆
![metaGer image](/20200215-search/07/metager.png)

metaGer actually managed a pretty good score here, thanks to a terse but accurate infobox linking to the Wikipedia article entitled: "_Salem, Oregon_".

#### Ecosia | ⭐☆☆☆
![Ecosia image](/20200215-search/07/ecosia.png)

Like Bing, Ecosia does not offer an instant answer, but the desired information can be found with a bit of parsing of the result summaries.

#### Swisscows | ⭐☆☆☆
![Swisscows image](/20200215-search/07/swisscows.png)

Swisscows also gets a brownie point because the desired information is pretty obvious from the summaries despite the lack of an instant answer box.

#### Searx | ⭐⭐⭐⭐
![Searx image](/20200215-search/07/searx.png)

Excellent. Searx offers not one but _two_ concise, informative infoboxes with statistics and maps, sourced from Wikidata and Wolfram|Alpha respectively.

#### Qwant | ⭐☆☆☆
![Qwant image](/20200215-search/07/qwant.png)

Only manual parsing of the results summaries yields an answer.

#### Yandex | ⭐☆☆☆
![Yandex image](/20200215-search/07/yandex.png)

Same thing with Yandex, although after the first two results it really gets out into the weeds due to it attempting to read into my search intent and failing miserably.

#### Mojeek | ☆☆☆☆
![Mojeek image](/20200215-search/07/mojeek.png)

Ouch. An embarassing fail for Mojeek, with a salacious and irrelevant news item, no other infoboxes, and completely wrong search results.


### Instant Answer Request #4: `$5/lb in gbp/kg`
This one is extremely difficult. First, the search engine has to parse a plethora of symbols and acronyms, then it has to access up-to-date currency exchange values, and then it has to convert between different units of measurement. It will be extremely impressive if any of the search engines figure this out.

#### Google | ⭐⭐⭐⭐
![Google image](/20200215-search/08/google.png)

Aced it. Very well done. Google is Google.

#### Startpage | ☆☆☆☆
![Startpage image](/20200215-search/08/startpage.png)

Startpage failed this test completely. It has become abundantly clear that Startpage offers similar (but not identical) search results sourced from Google, but that definitely does _not_ apply to instant answers.

#### Bing | ☆☆☆☆
![Bing image](/20200215-search/08/bing.png)

Bing failed to provide an instant answer.

#### DuckDuckGo | ☆☆☆☆
![DDG image](/20200215-search/08/ddg.png)

Extremely disappointing. This is a notable failure, as it shows how DuckDuckGo has actually regressed in functionality compared to my [2014 test](https://libretechtips.gitlab.io/review-duckduckgo-may-2014-redesign-compared-to-google-bing-yandex/), in which DDG automatically harnessed the power of Wolfram|Alpha to convert `mexican pesos / kg in usd/lb`. Unfortunately, since then DDG's partnership with Wolfram|Alpha has ended, and consequentially instant answers have taken a major hit.

#### metaGer | ☆☆☆☆
![metaGer image](/20200215-search/08/metager.png)

Unsurprisingly, metaGer failed to provide an instant answer.

#### Ecosia | ☆☆☆☆
![Ecosia image](/20200215-search/08/ecosia.png)

Ecosia also failed to provide an instant answer.

#### Swisscows | ☆☆☆☆
![Swisscows image](/20200215-search/08/swisscows.png)

Swisscows also failed to provide an instant answer.

#### Searx | ⭐⭐⭐⭐
![Searx image](/20200215-search/08/searx.png)

Nailed it! Although Searx does not officially partner with Wolfram|Alpha, it does pull results from its deep computational knowledge, and Searx summarizes the results in neat infoboxes. It should be noted that currently the Wolfram|Alpha engine is listed under the "Scientific" category of engines available to Searx, so it needs to be enabled and also that category must be set to display by default on all searches for optimal results. I also found that the other engines listed under the "Scientific" category tended to clutter up the Searx results with irrelevant scientific whitepapers and infoboxes, so as a workaround I disabled all the scientific engines except Wolfram|Alpha. I would strongly prefer for Searx to move Wolfram|Alpha to the "General" category, or else hardwire it as an "answerer", so that results from the dedicated scientific search engines can also be accessed independently. But the value of Wolfram|Alpha can not be overemphasized for excellent instant answers, as proven in this test.

#### Qwant | ☆☆☆☆
![Qwant image](/20200215-search/08/qwant.png)

Qwant failed to provide an instant answer.

#### Yandex | ☆☆☆☆
![Yandex image](/20200215-search/08/yandex.png)

Yandex also failed to provide an instant answer.

#### Mojeek | ☆☆☆☆
![Mojeek image](/20200215-search/08/mojeek.png)

Mojeek was particularly irrelevant and unhelpful with this query.


## Criterion #3: Other characteristics
**(Weight: 10%)**

#### Google | ⭐⭐☆☆

As the juggernaut of the search engine field, Google, with its massive search indexes and almost boundless comprehension of the Worldwide Web, combined with uncanny artificial intelligence, would appear to be practically invincible. And yet, over the years, complaints swirling around Google are on the uptick. The outcry tends to center on ethical issues that I prefer to avoid on this site. But even from a purely pragmatic standpoint, I have become increasingly frustrated with Google's tendency to ignore or misinterpret search query terms. Stop second-guessing me, Google! I know how to spell and how to use a search engine. Unless the query is exceptionally vague, consisting of maybe one or two totally ambiguous terms, a good search engine should never ignore terms or substitute them for others. Fortunately, at least Google does respect "words" "or" "phrases surrounded by quotes" to force it to honor all of the requested search terms. Another issue is that Google's favoring of commercial results (to say nothing of its core advertisements that are sprinkled in with organic results) feels unfair and tends to leave me with a lingering doubt that there could be more relevant pages out there that Google chooses to not offer me because they're not specifically linked with e-commerce. Although I am not dogmatically opposed to advertisements, certain queries on Google present an overwhelming barrage of ads, which is why an adblocker was enabled for the tests in this article. Google also gives excessive importance to Youtube videos and social media results, which usually fails to improve the quality of the results and just adds clutter.

#### Startpage | ⭐☆☆☆

I wanted to like Startpage, but I just can't warm up to it. It feels like it exhibits most of the bad traits of Google and few of the good ones. On the positive side, it does offer access to Google's massive index and uncanny search logic, albeit not the most current information set, as proven in this review. Like Google, Startpage respects "words" "and" "phrases surrounded by quotes", and it also has very good date filters for honing down the results to a specific time period. But I don't like the fact that Startpage is completely dependent on Google's willingness to extend its partnership with it, which would completely nullify Startpage's _raison d'être_ if it were ever severed. And finally, the dearth of instant answers on Startpage detracts significantly from its everyday usefulness as a go-to search engine.

#### Bing | ⭐☆☆☆

Bing is shockingly deficient considering its financial and technical underpinnings with a behemoth like Microsoft. For most queries, Bing was _laughably_ slow to return results. Even the public Searx instances I tested, which are probably running on an overloaded shared server somewhere and which pull results from a myriad of sources and aggregate them, were notably faster than Bing. The Bing engine is also notable for being one of the few in this test that does not strictly respect "words" "or" "phrases surrounded by quotes". This annoyance, combined with its tendency to ignore or misinterpret the intent of search terms, results in some exceptionally irrelevant and bizarre search results. Bing's crawler bot is one of the most active ones out there, and yet, it rarely finds results that are unique from Google's.

#### DuckDuckGo | ⭐⭐☆☆

I used to be a fan of DuckDuckGo, but its inferior performance compared to other options on this list is undeniable. It must be recognized that generating the big data required to assemble and maintain a relevant search index of the Worldwide Web is a gargantuan task, and therefore it is understandable that a startup like DuckDuckGo is the underdog. And yet, DuckDuckGo seems to be resting on its laurels and taking maximum advantage of the easy results that it sources from Bing, without making any progress whatsoever in including unique organic results from its own crawler. Furthermore, even if DuckDuckGo is beholden to Bing's search index, it could significantly differentiate itself by using better logic to avoid many of Bing's annoyances, such as not ignoring or changing search terms. But it doesn't; instead it behaves essentially the same as Bing. DDG's date filters are rudimentary and not at all effective. DDG has also regressed _significantly_ in its prowess at giving instant answers since terminating its agreement with Wolfram|Alpha. DuckDuckGo earns a slightly higher score than Bing by virtue of being faster, prettier, and honoring "words" "and" "phrases in quotation marks". I'm still rooting for it to improve, but judging from its lack of progress during the past six years since my last test, a major breakthrough is looking increasingly less likely.

#### metaGer | ⭐☆☆☆

Apart from its privacy focus, metaGer fails to make a convincing argument for using it over the other options in this review. It feels like it offers mainly regurgitated Bing results, with poorer or non-existent instant answers. One redeeming feature is its ability to filter results by date, but filtering results during the last year is unfortunately not available.

#### Ecosia | ⭐☆☆☆

Ecosia also fails to prove its mettle. It makes no pretenses of offering independent search results, instead depending completely on Bing's infrastructure, while offering no perceivable benefits in relevancy, and exhibiting very poor performance at offering instant answers. Like metaGer, it has the same inadequate options for filtering results by date.

#### Swisscows | ⭐⭐☆☆

Swisscows has some good ideas, such as the semantic meanings cloud. Its date filters are slightly more comprehensive. It has laudable privacy principles, and it at least is trying to create its own search index. But for now, Swisscows underperforms, and in practice it fails to signigicantly differentiate itself from Bing's underwhelming performance.

#### Searx | ⭐⭐⭐☆

As proof of my very high opinion that I developed for Searx, I have actually set it as the default search engine on my personal web browser. Searx was a huge surprise. Let's get a few negative points out of the way first: Searx is a meta search engine, so it doesn't have its own independent index or crawler, and it depends entirely on other search engines. As a result, it can sometimes get temporarily blocked by those engines if they detect Searx as a bot. This is especially common on the most popular public instances of Searx. It also lacks some of the polish of the big players in terms of interface refinement. But apart from that, Searx feels like it offers the best of both worlds: one-stop access to a myriad of general and specialized search engines, including ad-free direct access to Google's complete and current index, plus optionally weighing in results from independent players such as Mojeek, Yandex, Bing, and even Bing dependents like DuckDuckGo, all free of ads. It supports the same range of advanced search features that its source engines offer, such as honoring "quoted" "words" and date filters. Additionally, it presents smart, information-dense and relevant instant answers, including direct access to the gold standard Wolfram|Alpha engine. Searx offers a breath of fresh air in an internet environment dominated by Google and incompetent Bing clones.

#### Qwant | ⭐☆☆☆

Qwant was another huge disappointment. Despite having its own crawler and index, its results seemed heavily skewed toward the unsatisfactory tendancies of Bing. Its date filters are less than satisfactory, and in general it left me with more frustrations than successes.

#### Yandex | ☆☆☆☆

Yandex has very few redeeming qualities. For a large corporation, Yandex's performance was exceptionally incompetent. Results were often bizarrely irrelevant, and it offers no unique specil features. Indeed, its date and language filtering is insufficient, and it had one of the worst instant answer showings in this entire group. And to top it off, Yandex doesn't even offer good privacy policies for concerned users.

#### Mojeek | ⭐⭐☆☆

Mojeek impressed me with of its recognition of the importance of developing a unique, independent search index. It earns two stars for this very laudable goal, and I sincerely hope that it continues to improve. But for now, its results were generally subpar, and it sorely lacks important features such as "quoted phrases" ~~and date filters~~. ***EDIT:*** _Many thanks to [u/FjjB](https://www.reddit.com/user/FjjB/) on Reddit for letting me know that Mojeek does indeed offer [search operators](https://www.mojeek.com/support/search-operators.html) that allow for rather comprehensive date searches. So my only suggestion in that respect would be to add it to a more discoverable "Date filters" menu in a prominent UI location._



# Summary

Here are the final rankings of the weighted averages from this complete battery of tests: 

|Search Engine|Score (out of 4 stars)
|--|--|
Searx|3.53
Google| 2.98
Bing | 1.60
DuckDuckGo | 1.48
Startpage | 1.45
Swisscows | 1.33
Mojeek | 1.18
Ecosia | 1.15
Qwant | 0.93
Yandex | 0.83
metaGer | 0.78



## ✍ Share your thoughts on this article!
**➠[https://gitlab.com/libretechtips/libretechtips.gitlab.io/issues/1](https://gitlab.com/libretechtips/libretechtips.gitlab.io/issues/1)**



##### Footnotes:
