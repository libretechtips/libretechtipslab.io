---
title: "The best WYSIWYG option for Drupal is no WYSIWYG"
date: 2012-12-04
tags: ["tips", "drupal"]
---

What-You-See-Is-What-You-Get (WYSIWYG) editors for Drupal are abundant. Good ones are not. Most WYSIWYG editors for Drupal produce messy markup and don’t respect the site’s CSS theme. Additionally, many of them don’t integrate well with Drupal and cause problems on certain browsers. Worse yet, almost none of them work on Apple and Android tablets and smartphones. There are a few decent WYSIWYG editors for Drupal that can produce decent markup if the writer uses the tool properly. But they are often abused by users who insist on typing up their post in Microsoft Word and then pasting it into the WYSIWYG editor, resulting in awfully messy code that renders terribly. What can be done to avoid these multiple issues? The answer: Don’t use WYSWIYG editors. Now, a Drupal site without WYSIWYG editing does not mean that all users need to become HTML gurus. To the contrary, there are several options for creating content in Drupal in an easy and clean way that doesn’t use messy WYSIWYG editors.

Textile markup
--------------

The first key to the solution is the [Textile](http://www.booked.net/textism.html) [markup language](https://en.wikipedia.org/wiki/Textile_(markup_language)). Textile could be see as a translation layer between the user and HTML. The problem with HTML is that it is somewhat confusing for non-technical users. And even for those who understand it reasonably well, it is time consuming to type `<code>HTML tags</code>` while trying to write up an article. In my case, I prefer to concentrate on the content that I am writing instead of trying to remember to close my `<strong> tags`. So Textile serves as a middle layer for marking up text with fairly intuitive and brief tags. Then, when the article is saved, the Textile engine takes care of the rest and serves up a beautifully rendered page of clean, valid HTML. If you prefer, you can even type the article in a text editor and then paste it into Drupal. Or, you can save backup versions of your article in text format wherever you want apart from your website. This makes for a fast and efficient method that eliminates the distraction of formatting from the writing process. And better yet, it works on all devices, including the smartphones and tablets that usually choke on a WYSIWYG editor.

Textile markup is fairly simple and intuitive. For example, to make text bold, instead of enclosing the desired text with the `<strong>tag</strong>`, simply enclose the text in `*asterisks*` and it gets rendered as **bold**. Likewise, to set a line as a level 2 heading, just start the paragraph with:
`h2. Like This`
 (notice the space after the period) and Textile will generate the following code:
`<h2>Like This</h2>`
 Images can be inserted simply by enclosing the image URL in exclamation marks like this:
`!http://example.com/image.png!`
 There are many other simple markups for most needs.

Bueditor
--------

While Textile markup is simple and fairly easy to grasp, certain users still refuse to learn new concepts like markup languages. Or even in the case of a user like myself who prefers Textile, I sometimes forget the less common Textile markups. Fortunately, [Bueditor](https://www.drupal.org/project/bueditor) nicely fills that gap. Bueditor is *not* a WYSIWYG editor. Rather, it is a highly flexible generic editor that uses simple javascript buttons to inject code tags into a text field. Bueditor has different button sets for different markup formats, such as HTML. It does not, however, support Textile out of the box. Fortunately, it is fairly easy to add new buttons to inject any desired tag. So, I simply created my own Bueditor configuration for Textile.

### Configuring Bueditor to produce Textile markup

First, install the Bueditor module for Drupal. Then visit the Bueditor configuration page at `/admin/config/content/bueditor`. Click on the **import editor** link, and enter the name of your choice for the editor. In the **editor code** field, paste in the editor configuration for Textile.

**Important note:** Many web hosting providers enable a PHP configuration called “Magic Quotes”, which deletes backslashes in uploaded text to avoid security problems in poorly coded applications. If Magic Quotes is enabled, the above code won’t work properly without backslashes. In that case, contact your webhost for help to disable Magic Quotes.

Finally, back at the main Bueditor configuration page, assign the Bueditor configuration you just created to the desired roles.

Conclusion
----------

And that’s all. Textile with Bueditor is an excellent combination of simplicity and compatibility that provides a distraction-free environment conducive to writing quality content, while the system worries about the task of formatting. Many thanks to the authors of these fine tools.
